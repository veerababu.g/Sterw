/**
 * 
 */
package com.sterw.roadwarrior.exceptions;

/**
 * @author HCL
 *
 */
public class StrewException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String errorCode;

	
	
	public StrewException(String errorCode, String errorMsg) {		
		super(errorMsg);
		this.errorCode = errorCode;
	}

	public StrewException(String errorCode, Throwable cause) {
		super(errorCode, cause);
		this.errorCode = errorCode;
	}

	public StrewException(String message, Throwable error, String errorCode) {
		super(message,error);
		this.errorCode = errorCode;
		
	}
	public StrewException(Throwable e, String errorCode) {
		super(e);
		this.errorCode = errorCode;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	

	public String getErrorMsg() {
		return super.getMessage();
	}
}
