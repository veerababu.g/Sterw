package com.sterw.roadwarrior.dao;


import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.sterw.roadwarrior.constants.Constants;
import com.sterw.roadwarrior.constants.QuotationProgress;
import com.sterw.roadwarrior.domain.model.Policy;
import com.sterw.roadwarrior.domain.model.Pymt;
import com.sterw.roadwarrior.domain.model.Refer;
import com.sterw.roadwarrior.domain.model.Transaction;
import com.sterw.roadwarrior.domain.model.dto.DuplicateCustomerRequestDTO;
import com.sterw.roadwarrior.domain.model.dto.UBBStatusDTO;
import com.sterw.roadwarrior.repository.CustomerQuotationRepository;
import com.sterw.roadwarrior.repository.PolicyRepository;
import com.sterw.roadwarrior.repository.PymtRepository;
import com.sterw.roadwarrior.repository.ReferRepository;

/**
 * @author HCL
 *
 */
@Service
@Transactional
public class CustomerDAOImpl implements CustomerDAO {
	 private static final Logger logger = LogManager.getLogger(CustomerDAOImpl.class);
	@Autowired
	private CustomerQuotationRepository customerQuotationRepository;
	@Autowired 
	private PolicyRepository  policyRepository;
	@Autowired
	private PymtRepository pymtRepository;
	@Autowired
	private ReferRepository referRepository;
	
	@PersistenceContext
	EntityManager em;
	
	/**
	 * This method is used to create Quote  info 
	 * @param transaction - input the Transaction
	 * @return the transaction object
	 */
	public Transaction createQuote(final Transaction transaction) {
		try {
			String qtgcNumber;
			Query query;
			if(StringUtils.isEmpty(transaction)||StringUtils.isEmpty(transaction.getQuotationNo()) )
			{		 query = em.createNativeQuery(Constants.QUOTATION_SEQUENCE_QUERY);
					 qtgcNumber=query.getSingleResult().toString();
					 logger.info("*******Quotation Number from Databae function********"+qtgcNumber);
					 transaction.setQuotationNo(qtgcNumber);
			}else{
				qtgcNumber=transaction.getQuotationNo();
			}
            
			transaction.setCreatedDt(new Timestamp(new Date().getTime()));
			if(!StringUtils.isEmpty(transaction.getQtgcRiskVehs()))
				transaction.getQtgcRiskVehs().setQuotationNo(qtgcNumber);
	
			Transaction trans=customerQuotationRepository.saveAndFlush(transaction);
			customerQuotationRepository.updateRiskNumber(trans.getQtgcRiskVehs().getRiskId().toString(),trans.getQuotationNo());
			return trans ;
		} catch (Exception e) {
			logger.error("Exception raised Customer quotation insert in database"+e);
			throw e;
		}
		
	}
	
	/**
	 * This method is used to get Quotation base on quotation number 
	 * @param quotationNo - input the quotation number
	 * @return Transaction 
	 */
	@Override
	public Transaction getQuotation(String quotationNo) {
		try {
			return customerQuotationRepository.findOne(quotationNo);
		}catch (Exception e) {
			logger.error("Exception raised Customer quotation insert in database"+e);
			throw e;
		}
	}
	/**
	 * This method is used to persist Policy Info based on policy 
	 * @param policy - input the policy
	 * @return boolean  
	 */
	@Override
	public boolean persistPolicyInfo(Policy policy) {
		try {
			
			policyRepository.saveAndFlush(policy);
			updateQuotationProgress(QuotationProgress.POLICYISSUANCE.value(),policy.getQuotationNo());
			return true;
		} catch (Exception e) {
			logger.error("Exception raised policy update in database" + e);
			return false;
		}
	}
	/**
	 * This method is used to get the payment based on submission number
	 * @param submissionNumber - input the submission Number
	 * @return the payment details  based on submission Number   
	 */
	@Override
	public Pymt getPymt(String submissionNumber) {
		
		try{
			
			return pymtRepository.getPymtBySubmissionNo(submissionNumber);
		}
		catch(Exception e){
			logger.error("Exception raised fetch payment information in database"+e);
		}
		return null;
	}
	
	
	/**
	 * This method is used to update the payment status  
	 * @param pymt - input the Payment Object
	 * @return payment detail. 
	 */	
	@Override
	public Pymt paymentTranscation(Pymt pymt) {
		try{
			String quotationPymtStatus=QuotationProgress.PAYMENTFAIL.value();
			if(!StringUtils.isEmpty(pymt.getPymtStatus()) && pymt.getPymtStatus().equalsIgnoreCase(Constants.PAYMENTSTATUS)){
				quotationPymtStatus=QuotationProgress.PAYMENTSUCCESS.value();
				}
				updateQuotationProgress(quotationPymtStatus,pymt.getQuotationNo());
			return pymtRepository.saveAndFlush(pymt);
		}
		catch(Exception e){
			logger.error("Exception raised payment in database"+e);
			throw e;
		}
	}
	
	/**
	 * This method is used to update Quotation Progress  
	 * @param status,quotationNo - input the status and quotation Number
	 * @return void 
	 */	
	@Override
	public void updateQuotationProgress(String status, String quotationNo) {
		try{
		customerQuotationRepository.updateQuotationProgress(status, quotationNo);
		}
		catch(Exception e){
			logger.error("Exception raised updateQuotationProgress database"+e);
			throw e;
		}
	}
	
	/**
	 * This method is used to update UBB Status 
	 * @param transaction,referCode - input the transaction and referCode
	 * @return multiple submission numbers 
	 */
	@Override
	public Map<String,String> updateUBBStatus(UBBStatusDTO uBBStatusDTO) {
		Map<String,String> submissionNo= new HashMap<>();
			try{
				Pymt exitingPymt;
				if(!StringUtils.isEmpty(uBBStatusDTO)  && uBBStatusDTO.getCheckUBBStatus().equalsIgnoreCase(Constants.FALSE)){
					exitingPymt=pymtRepository.getPymtByQuotationNo(uBBStatusDTO.getQuotationNo());
				
					if(!StringUtils.isEmpty(exitingPymt) && !StringUtils.isEmpty(exitingPymt.getSubmissionNo())){
						submissionNo.put("SubmissionNumber",exitingPymt.getSubmissionNo());
					}
					else{
						Query query = em.createNativeQuery(Constants.SUBMISSION_SEQUENCE_QUERY);
						String submitionNumber=query.getSingleResult().toString();
						logger.info("******SubmitionNumber  from Databae function****"+submitionNumber);
						Pymt pymt= new Pymt();
							pymt.setQuotationNo(uBBStatusDTO.getQuotationNo());
							pymt.setSubmissionNo(submitionNumber);
							pymt.setPymtDate(new Timestamp(new Date().getTime()));
						pymtRepository.saveAndFlush(pymt);
						submissionNo.put("SubmissionNumber", pymt.getSubmissionNo());
					}
					if(StringUtils.isEmpty(referRepository.getReferByQuotationNo(uBBStatusDTO.getQuotationNo()))&&
							!StringUtils.isEmpty(uBBStatusDTO.getReferCode())){
						Refer refer= new Refer();
						refer.setQuotationNo(uBBStatusDTO.getQuotationNo());
						refer.setReferCode(uBBStatusDTO.getReferCode());
						referRepository.saveAndFlush(refer);
						
					}
					customerQuotationRepository.updateUBBStatus(uBBStatusDTO.getCheckUBBStatus(), uBBStatusDTO.getQuotationNo());
					return submissionNo ;
				}
				
			}
			catch(Exception e){
				logger.error("Exception raised update UBBStatus in database"+e);
				throw e;
				
			}
			return submissionNo;
	}
	
	/**
	 * This method is used to check Duplicate QuotationNo
	 * @param duplicateCustomerRequest - input DuplicateCustomerRequestDTO
	 * @return  List of quotation Numbers 
	 */	
	public Transaction checkDuplicateCustomer(DuplicateCustomerRequestDTO duplicateCustomerRequest){
		try{
			List<Transaction> trasactionList=customerQuotationRepository.getDuplicateCustomer(duplicateCustomerRequest.getClientId(), duplicateCustomerRequest.getEffDate(), duplicateCustomerRequest.getExpDate(), duplicateCustomerRequest.getProductCode(), duplicateCustomerRequest.getVehNo(), QuotationProgress.PAYMENTPENDING.value());
			if(!trasactionList.isEmpty()){
				return trasactionList.get(0);
			}else{
				return null;
			}
		}catch(Exception e){
			logger.error("Exception raised  checkDuplicateQuotationNo in database"+e);
			throw e;
			
		}
		
	 }


	/**
	 * This method is used to delete existing quotation detail
	 * @param transaction - input the {@link Transaction} object
	 * @return void
	 */
	@Override
	public void deleteQuotation(Transaction transaction) {
		customerQuotationRepository.delete(transaction);
		customerQuotationRepository.flush();
		
	}
	
}


	


