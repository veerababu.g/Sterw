package com.sterw.roadwarrior.dao;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.sterw.roadwarrior.domain.model.Policy;
import com.sterw.roadwarrior.domain.model.Pymt;
import com.sterw.roadwarrior.domain.model.Transaction;
import com.sterw.roadwarrior.domain.model.dto.DuplicateCustomerRequestDTO;
import com.sterw.roadwarrior.domain.model.dto.UBBStatusDTO;

/**
 * @author HCL
 *
 */
@Service
public interface CustomerDAO {
	/**
	 * This method is used to create Quote  info 
	 * @param transaction - input the Transaction
	 * @return the transaction object
	 */
Transaction createQuote(Transaction transaction);
/**
 * This method is used to get Quotation base on quotation number 
 * @param quotationNo - input the quotation number
 * @return Transaction 
 */
Transaction getQuotation(String quotationNo);
/**
 * This method is used to persist Policy Info based on policy 
 * @param policy - input the policy
 * @return boolean  
 */
boolean persistPolicyInfo(Policy policy);
/**
 * This method is used to update the payment status  
 * @param pymt - input the Payment Object
 * @return payment detail. 
 */	
Pymt paymentTranscation(Pymt pymt);
/**
 * This method is used to update UBB Status 
 * @param UBBStatusDTO
 * @return  submission number 
 */
Map<String,String> updateUBBStatus(UBBStatusDTO uBBStatusDTO);
/**
 * This method is used to check Duplicate Customer
 * @param duplicateCustomerRequest - input DuplicateCustomerRequestDTO
 * @return  Transaction
 */
Transaction checkDuplicateCustomer(DuplicateCustomerRequestDTO duplicateCustomerRequest);

/**
 * @param submisstionNo
 * @return
 */
Pymt getPymt(String submisstionNo);

/**
 * This method is used to delete existing quotation detail
 * @param transaction - input the {@link Transaction} object
 * @return void
 */
void deleteQuotation(Transaction transaction);
/**
 * This method is used to persist Policy Info based on policy 
 * @param policy - input the policy
 * @return boolean  
 */
void updateQuotationProgress(String status,String quotationNo);
}
