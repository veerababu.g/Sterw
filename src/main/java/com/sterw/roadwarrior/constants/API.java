package com.sterw.roadwarrior.constants;

/**
 * @author HCL
 *
 */
public enum API {
	TYPE("MODULARCN"),METHOD("INSERT"),SOURCESYSTEM("AZOL"),
	PRODUCTCODE("089507"),PRODUCTTYPE("RW"),CNOTETYPE("NW"),RISKLOCATED("WM"),
	RISKTYPE("PERSON"),PAYMENTMODE("CC"),COUNTRYCODE("MAL"),ISEPOLICY("N"),CNOTESTATUS("SUBMITTED");
	
	private String value;
	API(String value) {
        this.value = value;
    }

    public String value() {
    	
        return value;
    }
}
