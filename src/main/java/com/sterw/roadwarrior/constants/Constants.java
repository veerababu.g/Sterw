package com.sterw.roadwarrior.constants;

/**
 * 
 * This class contains the Constants to be used across the Application
 * 
 * @author HCL
 *
 */
public class Constants {
	private Constants(){
	}
	public static final String QUOTATION_SEQUENCE_QUERY = "SELECT 'CNAZ' + RIGHT('00000000'+cast(NEXT VALUE FOR dbo.QTGC_Transaction_Sequence as varchar(8)),8)";
	public static final String SUBMISSION_SEQUENCE_QUERY = "SELECT 'SBN' + RIGHT(year(getdate()),2) + '8'+ RIGHT('00000000'+cast(NEXT VALUE FOR dbo.QTGC_pymt_Sequence as varchar(8)),8)";
	public static final String API_KEY_DATE_FORMAT = "yyyyMMddHHmmssSSS";
	public static final String API_UPLOADTIME_DATE_FORMAT = "yyyyMMddHHmmss";
	public static final String SUCCESS="Success";
	public static final String FAIL="Fail";
	public static final String SUCCESSFUL="Successful";
	public static final String RECEIVEOK="RECEIVEOK";
	public static final String TRUE="true";
	public static final String FALSE="false";
	public static final String PAYMENTSTATUS="1";
	public static final String MALE="male";
	public static final String MALECODE="M";
	public static final String FEMALECODE="F";
	public static final String PAYMENT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String ENVDFAULT="local";
	public static final String ENVIRONMENT ="ENVIRONMENT";
	public static final String SUBMISSIONNUMBER ="SubmissionNumber";
	public static final String PAYMENTFAIL ="Payment fail";
	public static final String PAYMENTSUCCESSFUL ="00";
	public static final String PAYMENTREFER= "https://www.mobile88.com/";
	
	public static final String GOOGLECAPTCHAURI="https://www.google.com/recaptcha/api/siteverify";
	public static final String GOOGLECAPTCHAKEY="6LdQwRoUAAAAAJfOu4Hks136JFqTZKSXeyqXBoWJ";
	
	
}
