package com.sterw.roadwarrior.constants;

/**
 * @author HCL
 *
 */
public enum QuotationProgress {
	POLICYISSUANCE("PolicyIssued"),PAYMENTSUCCESS("PaymentSuccess"),PAYMENTFAIL("PaymentFail"),PAYMENTPENDING("PaymentPending"),
	USERDETAILS("UserDetails"),PAYMENT("Payment");
	private String value;
	QuotationProgress(String value) {
        this.value = value;
    }

    public String value() {
    	
        return value;
    }
}
