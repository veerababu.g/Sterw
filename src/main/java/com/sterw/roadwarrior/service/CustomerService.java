package com.sterw.roadwarrior.service;

import java.text.ParseException;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.sterw.roadwarrior.domain.model.dto.DuplicateCustomerRequestDTO;
import com.sterw.roadwarrior.domain.model.dto.DuplicateCustomerResponseDTO;
import com.sterw.roadwarrior.domain.model.dto.GetApiSignatureRequestDTO;
import com.sterw.roadwarrior.domain.model.dto.PersistPolicyInfoDTO;
import com.sterw.roadwarrior.domain.model.dto.PymtDTO;
import com.sterw.roadwarrior.domain.model.dto.QuotationResponseDTO;
import com.sterw.roadwarrior.domain.model.dto.TransactionDTO;
import com.sterw.roadwarrior.domain.model.dto.UBBStatusDTO;
@Service
public interface CustomerService {
	/**
	 * This method is used to create QuoteInfo based on input parameters 
	 * @param transactionDTO - input the transactionDTO
	 * @return the quotation QuotationResponseDTO
	 * @throws Exception
	 */
	QuotationResponseDTO createQuoteService(TransactionDTO transactionDTO);

	/**
	 * This method is used to get Quotation based on input parameters 
	 * @param quotationNo - input the transactionDTO
	 * @return the quotationNo QuotationResponseDTO
	 * @throws Exception
	 */
	TransactionDTO getQuotation(String quotationNo);

	/**
	 * This method is used to persist Policy Info based on input parameters 
	 * @param persistPolicyInfoDTO - input the persistPolicyInfoDTO
	 * @return policy info
	 * @throws Exception
	 */
	boolean persistPolicyInfo(PersistPolicyInfoDTO persistPolicyInfoDTO);

	/**
	 * This method is used to persist payment status based on input parameters 
	 * @param pymtDTO - input the PymtDTO
	 * @return PayMent Status
	 * @throws Exception
	 */
	PymtDTO persistPaymentIfo(PymtDTO pymtDTO);
	/**
	 * This method is used to update UBB status based on input parameters 
	 * @param uBBStatusDTO - input the UBBStatusDTO
	 * @return refer code
	 */	
	Map<String,String> updateUBBStatus(UBBStatusDTO uBBStatusDTO);

	/**
	 * This method is used to get Access Token 
	 * @return the access token in the response entity object.
	 */
	ResponseEntity<Object> getAccessToken();
	public String getGoogleAPICaptcha(String responseKey);
	DuplicateCustomerResponseDTO checkDuplicateCustomerService(DuplicateCustomerRequestDTO duplicateCustomerRequest)throws ParseException;

	/**
	 * This method is used to update Quotation Progress based on input quotationNo 
	 * @param quotationNo - input the quotation Number
	 * @return QuotationProgress based on PAYMENTPENDING
	 */
	void updateQuotationProgress(String quotationNo);

	GetApiSignatureRequestDTO getPaymentRequestSignature(GetApiSignatureRequestDTO getApiSignatureRequestDTO);
	
	
}
