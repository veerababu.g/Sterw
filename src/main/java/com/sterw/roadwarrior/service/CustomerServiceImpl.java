package com.sterw.roadwarrior.service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sterw.roadwarrior.api.RetrieveQuotePaymentInfoDTO;
import com.sterw.roadwarrior.constants.Constants;
import com.sterw.roadwarrior.constants.QuotationProgress;
import com.sterw.roadwarrior.dao.CustomerDAO;
import com.sterw.roadwarrior.domain.model.Cover;
import com.sterw.roadwarrior.domain.model.CoverFee;
import com.sterw.roadwarrior.domain.model.Policy;
import com.sterw.roadwarrior.domain.model.Pymt;
import com.sterw.roadwarrior.domain.model.SubCover;
import com.sterw.roadwarrior.domain.model.Transaction;
import com.sterw.roadwarrior.domain.model.dto.DuplicateCustomerRequestDTO;
import com.sterw.roadwarrior.domain.model.dto.DuplicateCustomerResponseDTO;
import com.sterw.roadwarrior.domain.model.dto.GetApiSignatureRequestDTO;
import com.sterw.roadwarrior.domain.model.dto.PersistPolicyInfoDTO;
import com.sterw.roadwarrior.domain.model.dto.PymtDTO;
import com.sterw.roadwarrior.domain.model.dto.QuotationResponseDTO;
import com.sterw.roadwarrior.domain.model.dto.TransactionDTO;
import com.sterw.roadwarrior.domain.model.dto.UBBStatusDTO;
import com.sterw.roadwarrior.exceptions.StrewException;
import com.sterw.roadwarrior.util.AllianzUtil;

@Service

public class CustomerServiceImpl implements CustomerService {
	private static final Logger logger = LogManager.getLogger(CustomerServiceImpl.class);
	@Autowired
	private CustomerDAO customerServiceDAO;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	ObjectMapper objectMaper;
	@Autowired
	private AllianzUtil allianzUtil;
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
	/**
	 * This method is used to create QuoteInfo based on input parameters 
	 * @param transactionDTO - input the transactionDTO
	 * @return the quotation QuotationResponseDTO
	 * @throws Exception
	 */
	public QuotationResponseDTO createQuoteService(TransactionDTO transactionDTO) {

		try {

			modelMapper = new ModelMapper();
			modelMapper.getConfiguration().setAmbiguityIgnored(true);
			transactionDTO.setQuotationProgress(QuotationProgress.USERDETAILS.value());
			if(!StringUtils.isEmpty(transactionDTO.getQuotationNo())){
				Transaction transaction = customerServiceDAO.getQuotation(transactionDTO.getQuotationNo());
				if( !StringUtils.isEmpty(transaction) &&
					!transaction.getQuotationProgress().contains(QuotationProgress.PAYMENT.value())&&
					!transaction.getQuotationProgress().contains(QuotationProgress.POLICYISSUANCE.value()))
						customerServiceDAO.deleteQuotation(transaction);
				else
					transactionDTO.setQuotationNo("");
			}
			Transaction transaction = modelMapper.map(transactionDTO, Transaction.class);
			logger.info("QTGC Transaction Mapped Object::"+objectMaper.writeValueAsString(transaction));
			transaction=customerServiceDAO.createQuote(transaction);
			return getCreateQuotationResponse(transaction);
		} catch (Exception e) {
			logger.error("Exception raised Customer quotation service response" + e);
			throw new StrewException("Exception raised Customer quotation service response",e);
		}
	}
	/**
	 * This method is used to get Quotation based on input parameters 
	 * @param quotationNo - input the transactionDTO
	 * @return the quotationNo QuotationResponseDTO
	 * @throws Exception
	 */
	public TransactionDTO getQuotation(String quotationNo) {
		try {
			logger.info("getQuotation entity input: {}", quotationNo);
			Transaction transaction = customerServiceDAO.getQuotation(quotationNo);
			return modelMapper.map(transaction, TransactionDTO.class);
		} catch (Exception e) {
			logger.error("Exception raised retriving Customer quotation service response" + e);
			throw new StrewException("Exception raised retriving Customer quotation service response",e);
		}

	}
	/**
	 * This method is used to persist Policy Info based on input parameters 
	 * @param persistPolicyInfoDTO - input the persistPolicyInfoDTO
	 * @return policy info
	 * @throws Exception
	 */
	public boolean persistPolicyInfo(PersistPolicyInfoDTO persistPolicyInfoDTO) {
		try {
			Policy policy = modelMapper.map(persistPolicyInfoDTO, Policy.class);
			logger.info("persistPolicyInfo entity input: {}", objectMaper.writeValueAsString(policy));
			return customerServiceDAO.persistPolicyInfo(policy);
		} catch (Exception e) {
			logger.error("Exception raised update policy status service response" + e);
			throw new StrewException("Exception raised update policy status service response",e);
		}
	}


	/**
	 * This method is used to persist payment status based on input parameters 
	 * @param pymtDTO - input the PymtDTO
	 * @return PayMent Status
	 * @throws Exception
	 */
	@Value("${api.submit.payment.url}")
	private String submitPaymentAPIURL;
	@Value("${ipay.epayment.enquiry}")
	private String ipayEnquiryURI;
	public PymtDTO persistPaymentIfo(PymtDTO pymtDTO){
		PymtDTO response = new PymtDTO();
		try {
			response.setPymtStatus(Constants.FAIL);
			logger.info("payment  input dto: {}", objectMaper.writeValueAsString(pymtDTO));
			Pymt existPymt = customerServiceDAO.getPymt(pymtDTO.getSubmissionNo());
			pymtDTO.setPymtDate(new Timestamp(new Date().getTime()));
			pymtDTO.setQuotationNo(existPymt.getQuotationNo());
			TransactionDTO transactionDTO = getQuotation(pymtDTO.getQuotationNo());
			transactionDTO.setPymt(pymtDTO);
			logger.info("transactionDTO: {}", objectMaper.writeValueAsString(transactionDTO));
			logger.info("Prem Amount from DB"+allianzUtil.getParseBigDecimal(transactionDTO.getPremDueRounded().toString()));
			logger.info("Prem Amount from IPAY"+allianzUtil.getParseBigDecimal(pymtDTO.getPymtAmt().toString()));
			GetApiSignatureRequestDTO getApiSignatureRequestDTO= new GetApiSignatureRequestDTO();
			getApiSignatureRequestDTO.setAmount((null!=transactionDTO.getPremDueRounded())?transactionDTO.getPremDueRounded().toString():"");
			getApiSignatureRequestDTO.setSubmissionNo(pymtDTO.getSubmissionNo());
			getApiSignatureRequestDTO.setStatus(pymtDTO.getPymtStatus());
			getApiSignatureRequestDTO.setPaymentId(pymtDTO.getPymtNumber());
			
			
			if(allianzUtil.checkPaymentEnquiry(pymtDTO,ipayEnquiryURI,getPaymentResponseSignature(getApiSignatureRequestDTO))){
						if (!StringUtils.isEmpty(existPymt.getQuotationNo())&&
								!StringUtils.isEmpty(existPymt)&&
								StringUtils.isEmpty(existPymt.getPymtStatus())){
							logger.info("Check"+allianzUtil.getParseBigDecimal(pymtDTO.getPymtAmt().toString()).equals(allianzUtil.getParseBigDecimal(transactionDTO.getPremDueRounded().toString())));
								Pymt pymt = modelMapper.map(pymtDTO, Pymt.class);
								pymt = customerServiceDAO.paymentTranscation(pymt);								
								response.setPymtStatus(Constants.SUCCESSFUL);
									if (null != pymt.getPymtStatus() && pymt.getPymtStatus().equalsIgnoreCase(Constants.PAYMENTSTATUS)) {
											// API Call
											RetrieveQuotePaymentInfoDTO retrieveQuotePaymentInfo = allianzUtil
													.getSubmitPaymentRequest(transactionDTO);
											logger.info("Submit paln and payment api  input request: {}",
													objectMaper.writeValueAsString(retrieveQuotePaymentInfo));
											Map<String, String> tokenBody = (Map<String, String>) getAccessToken().getBody();
											logger.info("Token from API body" + getAccessToken().getBody() + "Token::::"
													+ tokenBody.get("access_token"));
											ResponseEntity<String> apiResponse = allianzUtil.invokeSubmitPaymentAPI(
													retrieveQuotePaymentInfo, tokenBody.get("access_token"), submitPaymentAPIURL);
											logger.info("Submit paln and payment api  response: {}", apiResponse);
											if (!apiResponse.getBody().equalsIgnoreCase(Constants.FAIL))
												response.setSubmissionStatus(Constants.SUCCESSFUL);
											else{
												logger.info("Submit paln and payment connecting to OPUS api is faild"+ apiResponse);
												response.setSubmissionStatus(Constants.FAIL);
											}
										}
							
						} else {
							// because payment all ready proceed 
							response.setPymtStatus(Constants.SUCCESSFUL);
							if(existPymt.getPymtStatus().equalsIgnoreCase("1"))
								response.setSubmissionStatus(Constants.SUCCESSFUL);
						}
			}
		} catch (Exception e) {
			logger.error("Exception raised payment service response" + e);
			return response;
		}
		return response;
	}
	private GetApiSignatureRequestDTO getPaymentResponseSignature(GetApiSignatureRequestDTO getApiSignatureRequestDTO) {
		 MessageDigest md;
		 GetApiSignatureRequestDTO signatureResponseDTO= new GetApiSignatureRequestDTO();
		    try {
		    	String key=merchantKey+merchantCode+getApiSignatureRequestDTO.getPaymentId()+
		    			getApiSignatureRequestDTO.getSubmissionNo()
		    	+getApiSignatureRequestDTO.getAmount().replace(".", "").replace(",", "")+curreny+getApiSignatureRequestDTO.getStatus();
		      md = MessageDigest.getInstance("SHA");
		      md.update(key.getBytes("UTF-8")); // step 3
		      byte raw[] = md.digest(); // step 4
		      String hash = (new sun.misc.BASE64Encoder()).encode(raw); // step 5
		      signatureResponseDTO.setApiSignature(hash);
		      signatureResponseDTO.setCode(merchantCode);
		      logger.info("getPaymentSignature response::"+objectMaper.writeValueAsString(signatureResponseDTO));
		      return signatureResponseDTO; 
		    } catch (JsonProcessingException|UnsupportedEncodingException | NoSuchAlgorithmException e) {
		    	logger.error("Exception raised getPaymentSignature generation response" + e);
		    }
		   
		return signatureResponseDTO;
	}
	/**
	 * This method is used to update UBB status based on input parameters 
	 * @param uBBStatusDTO - input the UBBStatusDTO
	 * @return refer code
	 */	
	@Override
	public Map<String, String> updateUBBStatus(UBBStatusDTO uBBStatusDTO) {
		try {
			if (!StringUtils.isEmpty(uBBStatusDTO.getQuotationNo())) 
				return customerServiceDAO.updateUBBStatus(uBBStatusDTO);
			
		} catch (Exception e) {
			logger.error("Exception raised update UBBStatus service response" + e);
			throw e;
		}
		return new HashMap<>();
	}
	/**
	 * This method is used to update Quotation Progress based on input quotationNo 
	 * @param quotationNo - input the quotation Number
	 * @return QuotationProgress based on PAYMENTPENDING
	 */
	@Override
	public void updateQuotationProgress(String quotationNo) {
		try{
			customerServiceDAO.updateQuotationProgress(QuotationProgress.PAYMENTPENDING.value(), quotationNo);
		} catch (Exception e) {
			logger.error("Exception raised updateQuotationProgress service response" + e);
			throw e;
		}
	}
	
	/**
	 * This method is used to get Access Token 
	 * @return the access token in the response entity object.
	 */
	
	
	@Value("${api.url}")
	private String apiURL;
	@Value("${api.client.id}")
	private String apiClientId;
	@Value("${api.scret.code}")
	private String apiSecretCode;

	public ResponseEntity<Object> getAccessToken() {
		try {
			String url = apiURL;
			RestTemplate restTemplate = allianzUtil.restTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);
			MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
			body.add("client_id", apiClientId);
			body.add("client_secret", apiSecretCode);
			HttpEntity<?> entity = new HttpEntity<>(body, headers);
			ResponseEntity<Object> result = restTemplate.exchange(url, HttpMethod.POST, entity, Object.class);
			logger.info("response: {}", result);
			return result;
		} catch (Exception e) {
			logger.error("Exception raised token generation response" + e);
			return new ResponseEntity<>(new HashMap<>().put("status","Token Gernation failed"), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	public String getGoogleAPICaptcha(String responseKey) {
		try {
			String url = Constants.GOOGLECAPTCHAURI;
			RestTemplate restTemplate = allianzUtil.restTemplate();
			HttpHeaders headers = new HttpHeaders();
			MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
			body.add("secret", Constants.GOOGLECAPTCHAKEY);
			body.add("response", responseKey);
			HttpEntity<?> entity = new HttpEntity<>(body, headers);
			ResponseEntity<Object> result = restTemplate.exchange(url, HttpMethod.POST, entity, Object.class);
			logger.info("response: {}", objectMaper.writeValueAsString(result.getBody()));
			return objectMaper.writeValueAsString(result.getBody());
		} catch (Exception e) {
			logger.error("Exception raised Google Captcha Fail" + e);
			return "Google Captcha Fail";
		}

	}
	
	/**
	 * This method is used to update Quotation Progress based on input quotationNo 
	 * @param duplicateCustomerRequest - input the DuplicateCustomerRequestDTO
	 * @throws Exception
	 * @return true or false
	 * @throws ParseException 
	 */	
	public DuplicateCustomerResponseDTO checkDuplicateCustomerService(
			DuplicateCustomerRequestDTO duplicateCustomerRequest) throws ParseException {

		Transaction trasaction = customerServiceDAO.checkDuplicateCustomer(duplicateCustomerRequest);
		String isDuplicateCustomer= Constants.FALSE;
		if (!StringUtils.isEmpty(trasaction)) {
			 	Date modifiedDate= allianzUtil.getModifiedDate(trasaction.getCreatedDt().toString());
			    Date currentDate = new java.util.Date();
				if (currentDate.compareTo(modifiedDate) <= 0) {
					isDuplicateCustomer= Constants.TRUE;
				}
		} 
		DuplicateCustomerResponseDTO duplicateCustomerResponse = new DuplicateCustomerResponseDTO();
		duplicateCustomerResponse.setIsCustomerDuplicate(isDuplicateCustomer);
		return duplicateCustomerResponse;
	}
	/**
	 * This method is used to get Create Quotation Response based on input 
	 * @param transaction - input the Transaction
	 * @throws JsonProcessingException
	 * @return QuotationResponseDTO
	 */
	private QuotationResponseDTO getCreateQuotationResponse(Transaction transaction) throws JsonProcessingException{
		
		QuotationResponseDTO quotationCreateResponse = new QuotationResponseDTO();
		logger.info("QTGC Transaction response after creation::"+ objectMaper.writeValueAsString(transaction));
		quotationCreateResponse.setQuotationNumber(transaction.getQuotationNo());
		if (!StringUtils.isEmpty(transaction.getQtgcRiskVehs()))
			quotationCreateResponse.setRiskId(transaction.getQtgcRiskVehs().getRiskId());
		
		logger.info(objectMaper.writeValueAsString(transaction));
		if (!StringUtils.isEmpty(transaction.getQtgcCovers()) ) {
			Set<Cover> cover = transaction.getQtgcCovers();
			List<Integer> coverId = new ArrayList<>();
			List<Integer> subCoverId = new ArrayList<>();
			List<Integer> coverFeeList = new ArrayList<>();
			for (Cover cv : cover) {
				coverId.add(cv.getCoverNumber());
				getSubCoverIds(subCoverId,cv.getSubCover());
				getCoverFeeIds(coverFeeList,cv.getCoverFee());
			}
			quotationCreateResponse.setCoverId(coverId);
			quotationCreateResponse.setSubCoverId(subCoverId);
			quotationCreateResponse.setFeeId(coverFeeList);
		}
		return quotationCreateResponse;
	}

	/**
	 * @param coverFeeList
	 * @param coverFee
	 */
	private void getCoverFeeIds(List<Integer> coverFeeList, Set<CoverFee> coverFee) {
		if (!StringUtils.isEmpty(coverFee)) {
			for (CoverFee cf : coverFee) {
				coverFeeList.add(cf.getFeeNumber());
			}
		}
	}
	/**
	 * @param subCoverId
	 * @param subCover
	 */
	private void getSubCoverIds(List<Integer> subCoverId, Set<SubCover> subCover) {
		if (!StringUtils.isEmpty(subCover)) {
			for (SubCover sc : subCover) {
				subCoverId.add(sc.getSubCoverNumber());
			}
		}
		
	}
	
	@Value("${merchant.key}")
	private String merchantKey;
	@Value("${merchant.code}")
	private String merchantCode;
	@Value("${curreny}")
	private String curreny;
	
	@Override
	public GetApiSignatureRequestDTO getPaymentRequestSignature(GetApiSignatureRequestDTO getApiSignatureRequestDTO) {
		 MessageDigest md;
		 GetApiSignatureRequestDTO signatureResponseDTO= new GetApiSignatureRequestDTO();
		    try {
		    	String key=merchantKey+merchantCode+
		    			getApiSignatureRequestDTO.getSubmissionNo()
		    	+getApiSignatureRequestDTO.getAmount().replace(".", "").replace(",", "")+curreny;
		      md = MessageDigest.getInstance("SHA");
		      md.update(key.getBytes("UTF-8")); // step 3
		      byte raw[] = md.digest(); // step 4
		      String hash = (new sun.misc.BASE64Encoder()).encode(raw); // step 5
		      signatureResponseDTO.setApiSignature(hash);
		      signatureResponseDTO.setCode(merchantCode);
		      logger.info("getPaymentSignature response::"+objectMaper.writeValueAsString(signatureResponseDTO));
		      return signatureResponseDTO; 
		    } catch (JsonProcessingException|UnsupportedEncodingException | NoSuchAlgorithmException e) {
		    	logger.error("Exception raised getPaymentSignature generation response" + e);
		    }
		   
		return signatureResponseDTO;
	}
	

}
	

