package com.sterw.roadwarrior.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sterw.roadwarrior.domain.model.Policy;



public interface PolicyRepository extends JpaRepository<Policy, Integer> {

}
