package com.sterw.roadwarrior.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sterw.roadwarrior.domain.model.PaymtPK;
import com.sterw.roadwarrior.domain.model.Pymt;

public interface PymtRepository extends JpaRepository<Pymt, PaymtPK> {


	/**
	 * This method is used to get the payment details based on quotation number
	 * @param submissionNumber
	 * @return Pymt details
	 */
	
	@Query("select p from Pymt p where p.quotationNo = :quotationNo")
	public Pymt getPymtByQuotationNo(@Param("quotationNo") String quotationNo);
	
	/**
	 * This method is used to give the payment details based on submission number
	 * @param submissionNumber
	 * @return Pymt details
	 */
	@Query("select p from Pymt p where p.submissionNo = :submissionNumber")
	public Pymt getPymtBySubmissionNo(@Param("submissionNumber") String submissionNumber);
}
