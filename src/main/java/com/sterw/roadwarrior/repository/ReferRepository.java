package com.sterw.roadwarrior.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sterw.roadwarrior.domain.model.Refer;

public interface ReferRepository extends JpaRepository<Refer, Integer> {
	/**
	 * @param quotationNo
	 * @return
	 */
	@Query("select r from Refer r where r.quotationNo = :quotationNo")
	public Refer getReferByQuotationNo(@Param("quotationNo") String quotationNo);
}
