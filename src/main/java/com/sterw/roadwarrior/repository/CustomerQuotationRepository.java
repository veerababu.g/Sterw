package com.sterw.roadwarrior.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sterw.roadwarrior.domain.model.Transaction;

/**
 * @author HCL
 *
 */
public interface CustomerQuotationRepository extends JpaRepository<Transaction, String> {
	/**
	 * This method is used to update quotation progress based on status and quotation number
	 * @param submissionNumber
	 * @return 
	 */
	@Modifying
	@Query("update Transaction tr set tr.quotationProgress = :quotationProgress where tr.quotationNo = :quotationNo")
	int updateQuotationProgress(@Param("quotationProgress") String status, @Param("quotationNo") String quotationNo);
	
	@Query("select t from Transaction t join fetch t.qtgcRiskVehs v where t.quotationNo = v.quotationNo and t.clientNumber =:clientId and t.effDate = :effDate and t.expDate=:expDate and t.productCode =:productCode and v.vehNo =:vehNo and  t.quotationProgress=:status order by t.createdDt desc")
	List<Transaction> getDuplicateCustomer(@Param("clientId") String clientId,@Param("effDate") Date effDate,
			@Param("expDate") Date expDate,@Param("productCode") String productCode,@Param("vehNo") String vehNo,
			@Param("status") String status );
	@Modifying
	@Query("update Cover c  set c.riskNo=:riskNo  where c.quotationNo =:quotationNo and  c.riskNo=''")
	int updateRiskNumber(@Param("riskNo") String riskNo, @Param("quotationNo") String quotationNo);
	
	/**
	 * This method is used to update UBB Status based on status and quotation number
	 * @param status
	 * @param quotationNo
	 * @return 
	 */
	@Modifying
	@Query("update Transaction tr set tr.ubbStatus = :ubbStatus where tr.quotationNo = :quotationNo")
	int updateUBBStatus(@Param("ubbStatus") String status, @Param("quotationNo") String quotationNo);
	
	
	
}
