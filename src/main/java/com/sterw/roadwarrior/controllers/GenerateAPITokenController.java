package com.sterw.roadwarrior.controllers;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sterw.roadwarrior.service.CustomerService;

/**
 * @author HCL
 *
 */
@RequestMapping(value = "/api/v1")

@RestController
public class GenerateAPITokenController {
	 private static final Logger logger = LogManager.getLogger(GenerateAPITokenController.class);
	@Autowired
	CustomerService customerService;
	/** 
	 * This method is used to generate the access token
	 * @return ResponseEntity with Access token
	 */
	@RequestMapping(value = "/generateApiToken", method = RequestMethod.GET)
	public ResponseEntity<Object>  getAccessToken() {
		logger.info("Requested for acesscode{}");
		return customerService.getAccessToken();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/getGoogleCaptcha")
	public  String getGoogleCaptcha(@RequestBody Map<String, String> responseKey) {
			logger.info("Google Captcha input request: {}", responseKey.get("Response"));
			return customerService.getGoogleAPICaptcha(responseKey.get("Response"));
	
	}

}
