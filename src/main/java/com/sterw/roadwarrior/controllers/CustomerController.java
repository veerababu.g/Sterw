package com.sterw.roadwarrior.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sterw.roadwarrior.constants.Constants;
import com.sterw.roadwarrior.domain.model.dto.DuplicateCustomerRequestDTO;
import com.sterw.roadwarrior.domain.model.dto.DuplicateCustomerResponseDTO;
import com.sterw.roadwarrior.domain.model.dto.GenerateKeyDTO;
import com.sterw.roadwarrior.domain.model.dto.GetApiSignatureRequestDTO;
import com.sterw.roadwarrior.domain.model.dto.PersistPolicyInfoDTO;
import com.sterw.roadwarrior.domain.model.dto.PymtDTO;
import com.sterw.roadwarrior.domain.model.dto.QuotationResponseDTO;
import com.sterw.roadwarrior.domain.model.dto.ResponseDTO;
import com.sterw.roadwarrior.domain.model.dto.TransactionDTO;
import com.sterw.roadwarrior.domain.model.dto.UBBStatusDTO;
import com.sterw.roadwarrior.service.CustomerService;
import com.sterw.roadwarrior.util.AllianzUtil;
import com.sterw.roadwarrior.util.GenerateResponse;

/**
 *This Controller is used to perform customer quotation related activities  such as craeteQuoteInfo,payment,retrieve, Duplicate Customer Check etc.
 * @author HCL
 *
 */
@RestController
@CrossOrigin
@RequestMapping(value = "/api/v1")


public class CustomerController {
	@Autowired
	private CustomerService customerService;
	@Autowired
	ObjectMapper mapper;
	
	@Autowired
	GenerateKeyDTO generateKeyDTO;
	
	@Autowired
	private AllianzUtil allianzUtil;
	
	@Value("${privateKey}")
	private String genPrivateKey;
	
	@Value("${publicKey}")
	private String genPublicKey;
	
	
	private static final Logger logger = LogManager.getLogger(CustomerController.class);
	/**
	 * This method is used to create QuoteInfo based on input parameters 
	 * @param inputRequest - input the transactionDTO
	 * @return the ResponseEntity with quotationCreateResponseDTO
	 */
	@RequestMapping(value = "/createQuoteInfo", method = RequestMethod.POST)
	public  ResponseEntity<ResponseDTO<Object>> createQuote(@RequestBody TransactionDTO inputRequest) {
		QuotationResponseDTO quotationCreateResponseDTO = new QuotationResponseDTO();
		try {
		
			logger.info("Quotation create input request: {}", mapper.writeValueAsString(inputRequest));
			 Cipher cipher = Cipher.getInstance("RSA");
			// cipher.init(Cipher.DECRYPT_MODE, allianzUtil.readPrivateKey(generateKeyDTO.getPrivateKey()));
			 cipher.init(Cipher.DECRYPT_MODE, allianzUtil.readPrivateKey(genPrivateKey));
			 byte[] encodedPass = DatatypeConverter.parseBase64Binary(inputRequest.getPremiumDueEcrptAmt());
			 inputRequest.setPremDueRounded(allianzUtil.getParseBigDecimal(new String(cipher.doFinal(encodedPass), "UTF-8")));
			 quotationCreateResponseDTO = customerService.createQuoteService(inputRequest);
			return new ResponseEntity<>(GenerateResponse.getSuccessResponse(quotationCreateResponseDTO, Constants.SUCCESS),
					HttpStatus.CREATED);
		} catch (Exception e) {
			logger.error("Quotation create request is faild: {}", e);
			return new ResponseEntity<>(GenerateResponse.getSuccessResponse(inputRequest, Constants.FAIL),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * This method is used to get the quotation Details based on quotation Number
	 * @param quotationNo - input the quotation Number
	 * @return the ResponseEntity with transactionDTO
	 */
	
	@RequestMapping(method = RequestMethod.GET, value = "/retrieveQuotePaymentInfo/{quotationNo}")
	public  ResponseEntity<ResponseDTO<Object>> getQuotationDetails(@PathVariable("quotationNo") String quotationNo) {
		TransactionDTO transactionDTO = new TransactionDTO();
		try {
			
			logger.info("Customer quotation retrieve input request: {}", quotationNo);
			transactionDTO = customerService.getQuotation(quotationNo);
			return new ResponseEntity<>(GenerateResponse.getSuccessResponse(transactionDTO, Constants.SUCCESS), HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Customer quotation retrieve is faild: {}", e);
			return new ResponseEntity<>(GenerateResponse.getSuccessResponse(quotationNo, Constants.FAIL),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	/**
	 * This method is used to update Quotation Progress based on quotation Number
	 * @param quotationNo - input the quotation Number
	 * @return void
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/updateQuotationProgress/{quotationNo}")
	public void updateQuotationProgress(@PathVariable("quotationNo") String quotationNo) {
		
		try {
			logger.info("Customer updateQuotationProgress  input request: {}", quotationNo);
			customerService.updateQuotationProgress(quotationNo);
		} catch (Exception e) {
			logger.error("Customer quotation retrieve is faild: {}", e);
		}
	}
	
	/**
	 * This method is used to persist the policy info 
	 * @param persistPolicyInfo - input the policy details in the DTO
	 * @return the ResponseEntity with Success or Failure Status 
	 */	
	@RequestMapping(method = RequestMethod.POST, value = "/persistPolicyInfo")
	public ResponseEntity<ResponseDTO<Object>> persistPolicyInfo(@RequestBody PersistPolicyInfoDTO persistPolicyInfo) {

		try {
			logger.info("Persist policy info input Request: {}", mapper.writeValueAsString(persistPolicyInfo));
			boolean status = customerService.persistPolicyInfo(persistPolicyInfo);
			return new ResponseEntity<>(GenerateResponse.getSuccessResponse(status, Constants.SUCCESS), HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Persist policy info  is faild: {}", e);
			return new ResponseEntity<>(GenerateResponse.getSuccessResponse(false, Constants.FAIL),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * This method is used to submit Plan And Payment 
	 * @param pymtDTO - input the payment details in the DTO
	 * @return the  ResponseEntity with the Success or Failure Status 
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/submitPlanAndPayment")
	public ResponseEntity<ResponseDTO<Object>>submitPlanAndPayment(@RequestBody PymtDTO pymtDTO) {

		try {
			logger.info("Submit plan and payment input Request: {}"+ mapper.writeValueAsString(pymtDTO));
			if(!StringUtils.isEmpty(pymtDTO.getReferrer()) && pymtDTO.getReferrer().contains(Constants.PAYMENTREFER)){
			
				PymtDTO pymtDto = customerService.persistPaymentIfo(pymtDTO);
				return new ResponseEntity<>(GenerateResponse.getSuccessResponse(pymtDto, Constants.SUCCESS), HttpStatus.OK);
			}
		} catch (Exception e) {
			logger.error("Submit plan and payment is faild: {}", e);
		}
		return new ResponseEntity<>(GenerateResponse.getSuccessResponse(pymtDTO, Constants.FAIL),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * This method is used to update Payment Info 
	 * @param request - input the payment info into the request Object
	 * @return the PaymentInfo status
	 */
	
	@RequestMapping(method = RequestMethod.POST, value = "/updatePaymentInfo")
	public String updatePaymentInfo(HttpServletRequest request,HttpServletResponse response) {
		
		try {
			logger.info("*************Request recived from IPAY 88"+request.getRemoteHost());
			logger.info("Update paymentInfo input Request: {}", request);
				customerService.persistPaymentIfo(allianzUtil.getPymtFromHttpRequest(request));
				return Constants.RECEIVEOK;
	
		} catch (Exception e) {
			logger.error("Update payment Info is faild: {}" + e);
		}
		return "";
	}

	/**
	 * This method is used to update the UBB Status 
	 * @param uBBStatusDTO - input the status object uBBStatusDTO
	 * @return the ResponseEntity with Success or Fail status
 	 */
	@RequestMapping(method = RequestMethod.POST, value = "/updateUBBStatus")
	public ResponseEntity<ResponseDTO<Object>> updateUBBStatus(@RequestBody UBBStatusDTO uBBStatusDTO) {

		try {
			logger.info("Update UBBStatus input Request: {}", mapper.writeValueAsString(uBBStatusDTO));
			Map<String, String> status = customerService.updateUBBStatus(uBBStatusDTO);
			return new ResponseEntity<>(GenerateResponse.getSuccessResponse(status, Constants.SUCCESS), HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Update updateUBBStatus is faild: {}", e);
			return new ResponseEntity<>(GenerateResponse.getSuccessResponse(new HashMap<>(),Constants.FAIL),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * This method is used to check whether customer is duplicate or not 
	 * @param duplicateCustomerRequest - input the duplicate customer parameters  
	 * @return the  ResponseEntity with the Success or Failure Status 
	 */
	@RequestMapping(value = "/checkDuplicateCustomer", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<Object>> checkDuplicateCustomer(@RequestBody DuplicateCustomerRequestDTO duplicateCustomerRequest) {
		try {
			logger.info("Duplicate customer check input Request: {}", mapper.writeValueAsString(duplicateCustomerRequest));

			DuplicateCustomerResponseDTO duplicateCustomerResponse = customerService
					.checkDuplicateCustomerService(duplicateCustomerRequest);
			return new ResponseEntity<>(GenerateResponse.getSuccessResponse(duplicateCustomerResponse, Constants.SUCCESS),
					HttpStatus.OK);

		} catch (Exception e) {
			logger.error("Duplicate customer check request is faild: {}", e);
			return new ResponseEntity<>(GenerateResponse.getSuccessResponse(duplicateCustomerRequest, Constants.FAIL),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	/**
	 * @param getApiSignatureRequestDTO
	 * @return
	 */
	@RequestMapping(value = "/getPaymentSignature", method = RequestMethod.POST)
	public ResponseEntity<ResponseDTO<Object>> getPaymentSignature(@RequestBody GetApiSignatureRequestDTO getApiSignatureRequestDTO ) {
		
		
		try{
			logger.info("Requested for getPaymentSignature{}", mapper.writeValueAsString(getApiSignatureRequestDTO));
				return new ResponseEntity<>(GenerateResponse.getSuccessResponse(customerService.getPaymentRequestSignature(getApiSignatureRequestDTO), Constants.SUCCESS),
						HttpStatus.OK);
			} catch (Exception e) {
				logger.error("getPaymentSignature is faild: {}", e);
				return new ResponseEntity<>(GenerateResponse.getSuccessResponse(getApiSignatureRequestDTO, Constants.FAIL),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
	}
	
	@RequestMapping(value = "/getPublicKey", method = RequestMethod.GET)
	public ResponseEntity<ResponseDTO<Object>> getPublicKey() {
		Map<String,String> publicKey= new HashMap<>();
		try {
			publicKey.put("PublicKey", genPublicKey);
			//publicKey.put("PrivateKey", generateKeyDTO.getPrivateKey());
			return new ResponseEntity<>(GenerateResponse.getSuccessResponse(publicKey, Constants.SUCCESS), HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Customer quotation retrieve is faild: {}", e);
			return new ResponseEntity<>(GenerateResponse.getSuccessResponse(publicKey, Constants.FAIL),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

		
		
	}
}
