package com.sterw.roadwarrior;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.RSAKeyGenParameterSpec;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.sterw.roadwarrior.constants.Constants;
import com.sterw.roadwarrior.domain.model.dto.GenerateKeyDTO;
import com.sterw.roadwarrior.filters.IPWhiteListFilter;


/**
 * 
 * @author HCL
 */
@Configuration
@EnableAutoConfiguration
@SpringBootApplication
@CrossOrigin
@ComponentScan({ "com.sterw"})
@EntityScan("com.sterw.roadwarrior.domain.model")


public class Application extends SpringBootServletInitializer {
	private KeyPairGenerator keyGen;
	private PrivateKey privateKey;
	private PublicKey publicKey;
	 public static void main(String[] args) {
	        SpringApplication.run(applicationClass, args);
	    }
	    @Override
	    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	    	return application.sources(applicationClass);
	    }
	    private static Class<Application> applicationClass = Application.class;
	    
	    @Bean
	    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
	    	String env=System.getProperty(Constants.ENVIRONMENT);
	    	if(StringUtils.isEmpty(env)){
	    		env=Constants.ENVDFAULT;
	    	}
	        String activeProfile = System.getProperty("spring.profiles.active", env);
	        String propertiesFilename = "application_" + activeProfile + ".properties";
	        PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
	        configurer.setLocation(new ClassPathResource(propertiesFilename));
	        return configurer;
	    }
	    @Value("${ipay.ip}")
		private String ipayIp;
	    @Value("${sterw.micro.ipay.backend.service}")
		private String updatePyamentService;
	    
	    @Bean
	    public FilterRegistrationBean authorizationFilter(){
	        FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
	        IPWhiteListFilter ipFilter= new IPWhiteListFilter();
	        filterRegBean.setFilter(ipFilter);
	        List<String> urlPatterns = new ArrayList<>();
	        urlPatterns.add(updatePyamentService);
	        filterRegBean.addInitParameter("x-forward-ip", ipayIp);
	        filterRegBean.setUrlPatterns(urlPatterns);
	        return filterRegBean;
	    }
	    private void generateSecureKeys() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException {
	    	this.keyGen = KeyPairGenerator.getInstance("RSA");
	    	RSAKeyGenParameterSpec spec = new RSAKeyGenParameterSpec(1024, RSAKeyGenParameterSpec.F4);
	    	SecureRandom random = new SecureRandom();
	    	keyGen.initialize(spec,random);
	    	this.keyGen.initialize(1024);
	    }

	    private void createKeys() {
	    	KeyPair pair = this.keyGen.generateKeyPair();
	    	this.privateKey = pair.getPrivate();
	    	this.publicKey = pair.getPublic();
	    }

	    private PrivateKey getPrivateKey() {
	    	return this.privateKey;
	    }

	    private PublicKey getPublicKey() {
	    	return this.publicKey;
	    }
	    @Bean
	    public GenerateKeyDTO keyGenerateAndReturnPublicKey(GenerateKeyDTO generateKeyDTO) {
	    	String publicKeyPEM ;
	    	String privateKeyPEM;
	    	
	    	try {
	    		this.generateSecureKeys();
	    		this.createKeys();
	    		// THIS IS PEM:
	    		  publicKeyPEM =  DatatypeConverter.printBase64Binary(getPublicKey().getEncoded());
		          privateKeyPEM = DatatypeConverter.printBase64Binary(getPrivateKey().getEncoded());
	            generateKeyDTO.setPrivateKey(privateKeyPEM);
	            generateKeyDTO.setPublicKey(publicKeyPEM);
	           
	        } catch (Exception e) {
	    		logger.error("Exception raised in keys encription"+e);
	    	}
	    	 return generateKeyDTO;
	    } 
}	   
