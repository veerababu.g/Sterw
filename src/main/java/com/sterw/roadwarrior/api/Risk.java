
package com.sterw.roadwarrior.api;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "RiskGrp"
})
public class Risk implements Serializable
{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("RiskGrp")
    private RiskGrp riskGrp;
  
	public RiskGrp getRiskGrp() {
		return riskGrp;
	}
	public void setRiskGrp(RiskGrp riskGrp) {
		this.riskGrp = riskGrp;
	}

  

}
