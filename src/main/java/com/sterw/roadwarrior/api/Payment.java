
package com.sterw.roadwarrior.api;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "PaymentMode",
    "PaymentBankRef",
    "PaymentId",
    "PaymentDate",
    "PaymentAmount",
    "SubmissionNo"
})
public class Payment implements Serializable
{

    /**
	 * 
	 */
	private static final long serialVersionUID = -6800172034605610089L;
	@JsonProperty("PaymentMode")
    private String paymentMode;
    @JsonProperty("PaymentBankRef")
    private String paymentBankRef;
    @JsonProperty("PaymentId")
    private String paymentId;
    @JsonProperty("PaymentDate")
    private String paymentDate;
    @JsonProperty("PaymentAmount")
    private String paymentAmount;
    @JsonProperty("SubmissionNo")
    private String submissionNo;
    
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getPaymentBankRef() {
		return paymentBankRef;
	}
	public void setPaymentBankRef(String paymentBankRef) {
		this.paymentBankRef = paymentBankRef;
	}
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getSubmissionNo() {
		return submissionNo;
	}
	public void setSubmissionNo(String submissionNo) {
		this.submissionNo = submissionNo;
	}

    

}
