
package com.sterw.roadwarrior.api;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "SubCoverCode",
    "SubCoverId",
    "SubPrem",
    "SubSi",
    "PlanCode"
})
public class SubCoverGrp implements Serializable
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("SubCoverCode")
    private String subCoverCode;
    @JsonProperty("SubCoverId")
    private String subCoverId;
    @JsonProperty("SubPrem")
    private String subPrem;
    @JsonProperty("SubSi")
    private String subSi;
    @JsonProperty("PlanCode")
    private String planCode;
   
	public String getSubCoverCode() {
		return subCoverCode;
	}
	public void setSubCoverCode(String subCoverCode) {
		this.subCoverCode = subCoverCode;
	}
	public String getSubCoverId() {
		return subCoverId;
	}
	public void setSubCoverId(String subCoverId) {
		this.subCoverId = subCoverId;
	}
	public String getSubPrem() {
		return subPrem;
	}
	public void setSubPrem(String subPrem) {
		this.subPrem = subPrem;
	}
	public String getSubSi() {
		return subSi;
	}
	public void setSubSi(String subSi) {
		this.subSi = subSi;
	}
	public String getPlanCode() {
		return planCode;
	}
	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

    

}
