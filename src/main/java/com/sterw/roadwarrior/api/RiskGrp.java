
package com.sterw.roadwarrior.api;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "RiskId",
    "RiskLocated",
    "RiskType",
    "RiskVeh",
    "RiskPerson",
    "Cover"
})
public class RiskGrp implements Serializable
{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("RiskId")
    private String riskId;
    @JsonProperty("RiskLocated") //discuss
    private String riskLocated;
    @JsonProperty("RiskType")//discuss
    private String riskType;
    
    @JsonProperty("RiskVeh")
    private RiskVeh riskVehs;
    
    @JsonProperty("RiskPerson")// discuss
    private RiskPerson riskPerson;
    
    @JsonProperty("Cover")
    private Cover cover;
   
	public String getRiskId() {
		return riskId;
	}
	public void setRiskId(String riskId) {
		this.riskId = riskId;
	}
	public String getRiskLocated() {
		return riskLocated;
	}
	public void setRiskLocated(String riskLocated) {
		this.riskLocated = riskLocated;
	}
	public String getRiskType() {
		return riskType;
	}
	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}
	
	public RiskPerson getRiskPerson() {
		return riskPerson;
	}
	public void setRiskPerson(RiskPerson riskPerson) {
		this.riskPerson = riskPerson;
	}
	public Cover getCover() {
		return cover;
	}
	public void setCover(Cover cover) {
		this.cover = cover;
	}
	public RiskVeh getRiskVehs() {
		return riskVehs;
	}
	public void setRiskVehs(RiskVeh riskVehs) {
		this.riskVehs = riskVehs;
	}

   

}
