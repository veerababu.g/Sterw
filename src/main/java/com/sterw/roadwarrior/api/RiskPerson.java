
package com.sterw.roadwarrior.api;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "IdType1",
    "IdValue1",
    "Name",
    "Gender",
    "Occupation",
    "OccupationDescription"
})
public class RiskPerson implements Serializable
{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("IdType1")
    private String idType1;
    @JsonProperty("IdValue1")
    private String idValue1;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Gender")
    private String gender;
    @JsonProperty("Occupation")
    private String occupation;
    @JsonProperty("OccupationDescription")
    private String occupationDescription;
   
	public String getIdType1() {
		return idType1;
	}
	public void setIdType1(String idType1) {
		this.idType1 = idType1;
	}
	public String getIdValue1() {
		return idValue1;
	}
	public void setIdValue1(String idValue1) {
		this.idValue1 = idValue1;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getOccupationDescription() {
		return occupationDescription;
	}
	public void setOccupationDescription(String occupationDescription) {
		this.occupationDescription = occupationDescription;
	}
    

   

}
