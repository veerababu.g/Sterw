
package com.sterw.roadwarrior.api;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Payment"
})
public class Payments implements Serializable
{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("Payment")
    private Payment payment;
    
	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}

    
}
