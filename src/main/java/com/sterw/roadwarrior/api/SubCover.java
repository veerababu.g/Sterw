
package com.sterw.roadwarrior.api;

import java.io.Serializable;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(JsonInclude.Include.NON_NULL)

public class SubCover implements Serializable
{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("SubCoverGrp")
    private Set<SubCoverGrp> subCoverGrp;
   
	public Set<SubCoverGrp> getSubCoverGrp() {
		return subCoverGrp;
	}
	public void setSubCoverGrp(Set<SubCoverGrp> subCoverGrp) {
		this.subCoverGrp = subCoverGrp;
	}

    

}
