
package com.sterw.roadwarrior.api;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * @author HCL
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CoverGrp"
})
public class Cover implements Serializable
{
	

    /**
	 * 
	 */
	private static final long serialVersionUID = 5352805449543778941L;
	@JsonProperty("CoverGrp")
    private CoverGrp coverGrp;
  
	public CoverGrp getCoverGrp() {
		return coverGrp;
	}
	public void setCoverGrp(CoverGrp coverGrp) {
		this.coverGrp = coverGrp;
	}

   

}
