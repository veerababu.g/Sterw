
package com.sterw.roadwarrior.api;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "VehicleNo",
    "VehicleModel",
    "VehicleSeat"
})
public class RiskVeh implements Serializable
{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("VehicleNo")
    private String vehicleNo;
    @JsonProperty("VehicleModel")
    private String vehicleModel;
    @JsonProperty("VehicleSeat")
    private String vehicleSeat;
    
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getVehicleModel() {
		return vehicleModel;
	}
	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}
	public String getVehicleSeat() {
		return vehicleSeat;
	}
	public void setVehicleSeat(String vehicleSeat) {
		this.vehicleSeat = vehicleSeat;
	}
    

}
