
package com.sterw.roadwarrior.api;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ContractNo",
    "ProductCode",
    "ProductType",
    "CNoteType",
    "AgentCode",
    "EffDate",
    "ExpDate",
    "InsuredName",
    "IdType1",
    "IdValue1",
    "Name",
    "DOB",
    "Gender",
    "MobileCode1",
    "MobileNo1",
    "Email",
    "AddressType",
    "AddressLine1",
    "AddressLine2",
    "AddressLine3",
    "Postcode",
    "City",
    "State",
    "Country",
    "ClOccupCode",
    "ClOccupDescription",
    "PolSi",
    "AnnualPrem",
    "GrossPrem",
    "RebatePct",
    "RebateAmt",
    "Stamp",
    "GstPct",
    "GstAmt",
    "BasicPrem",
    "NettPrem",
    "PremDue",
    "PremDueRounded",
    "Risk",
    "CoverNoteBenefits",
    "Payments"
})
public class Detail implements Serializable
{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("ContractNo")
    private String quotationNo;
    
    @JsonProperty("ProductCode")
    private String productCode;
    
    @JsonProperty("ProductType")
    private String productType;
    
    @JsonProperty("CNoteType")
    private String cNoteType;
    
    @JsonProperty("AgentCode")
    private String agentCode;
    
    @JsonProperty("EffDate")
    @JsonFormat
    (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date effDate;
    @JsonFormat
    (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonProperty("ExpDate")
    private Date expDate;
    @JsonProperty("InsuredName")
    private String insuredName;
    @JsonProperty("IdType1")
    private String clientType;
    @JsonProperty("IdValue1")
    private String clientNumber;
    
    @JsonProperty("Name")
    private String clientName;
    
    @JsonProperty("DOB")
    @JsonFormat
    (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date dob;
    @JsonProperty("Gender")
    private String gender;
    
    @JsonProperty("MobileCode1") //discuss empty
    private String mobileCode1;
    
    @JsonProperty("MobileNo1")
    private String mobileNo;
    @JsonProperty("IsEPolicy")
    private String isEPolicy;
    
    @JsonProperty("CNoteStatus")
    private String cNoteStatus;
    
    
    @JsonProperty("Email")
    private String email;
    @JsonProperty("AddressType")//discuss default R
    private String addressType;
    
    @JsonProperty("AddressLine1")
    private String address1;
    @JsonProperty("AddressLine2")
    private String address2;
    @JsonProperty("AddressLine3")
    private String address3;
    @JsonProperty("Postcode")
    private String postcode;
    @JsonProperty("City")
    private String city;
    @JsonProperty("State")
    private String state;
    @JsonProperty("Country")
    private String countryCode;
    
    @JsonProperty("ClOccupCode") 
    private String occupationCode;
    @JsonProperty("ClOccupDescription")
    private String occupation;
    
    @JsonProperty("PolSi")
    private String policySi;
    @JsonProperty("AnnualPrem")
    private String annualPrem;
    @JsonProperty("GrossPrem")
    private String grossPrem;
    @JsonProperty("RebatePct")
    private String rebatePct;
    @JsonProperty("RebateAmt")
    private String rebateAmt;
    @JsonProperty("Stamp")
    private String stamp;
    @JsonProperty("GstPct")
    private String gstPct;
    @JsonProperty("GstAmt")
    private String gstAmt;
    @JsonProperty("BasicPrem")
    private String basicPrem;
    @JsonProperty("NettPrem")
    private String nettPrem;
    @JsonProperty("PremDue")
    private String premDue;
    @JsonProperty("PremDueRounded")
    private String premDueRounded;
    @JsonProperty("Risk")
    private Risk risk;
    @JsonProperty("CoverNoteBenefits")
    private CoverNoteBenefits coverNoteBenefits;
    @JsonProperty("Payments")
    private Payments payments;
    
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getcNoteType() {
		return cNoteType;
	}
	public void setcNoteType(String cNoteType) {
		this.cNoteType = cNoteType;
	}
	public String getAgentCode() {
		return agentCode;
	}
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	public Date getEffDate() {
		return effDate;
	}
	public void setEffDate(Date effDate) {
		this.effDate = effDate;
	}
	public Date getExpDate() {
		return expDate;
	}
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
	public String getInsuredName() {
		return insuredName;
	}
	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}
	public String getClientType() {
		return clientType;
	}
	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
	public String getClientNumber() {
		return clientNumber;
	}
	public void setClientNumber(String clientNumber) {
		this.clientNumber = clientNumber;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMobileCode1() {
		return mobileCode1;
	}
	public void setMobileCode1(String mobileCode1) {
		this.mobileCode1 = mobileCode1;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getAddress3() {
		return address3;
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	public String getPostcode() {
		return postcode;
	}
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}
	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	/**
	 * @return the occupationCode
	 */
	public String getOccupationCode() {
		return occupationCode;
	}
	/**
	 * @param occupationCode the occupationCode to set
	 */
	public void setOccupationCode(String occupationCode) {
		this.occupationCode = occupationCode;
	}
	/**
	 * @return the occupation
	 */
	public String getOccupation() {
		return occupation;
	}
	/**
	 * @param occupation the occupation to set
	 */
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getPolicySi() {
		return policySi;
	}
	public void setPolicySi(String policySi) {
		this.policySi = policySi;
	}
	public String getAnnualPrem() {
		return annualPrem;
	}
	public void setAnnualPrem(String annualPrem) {
		this.annualPrem = annualPrem;
	}
	public String getGrossPrem() {
		return grossPrem;
	}
	public void setGrossPrem(String grossPrem) {
		this.grossPrem = grossPrem;
	}
	public String getRebatePct() {
		return rebatePct;
	}
	public void setRebatePct(String rebatePct) {
		this.rebatePct = rebatePct;
	}
	public String getRebateAmt() {
		return rebateAmt;
	}
	public void setRebateAmt(String rebateAmt) {
		this.rebateAmt = rebateAmt;
	}
	public String getStamp() {
		return stamp;
	}
	public void setStamp(String stamp) {
		this.stamp = stamp;
	}
	public String getGstPct() {
		return gstPct;
	}
	public void setGstPct(String gstPct) {
		this.gstPct = gstPct;
	}
	public String getGstAmt() {
		return gstAmt;
	}
	public void setGstAmt(String gstAmt) {
		this.gstAmt = gstAmt;
	}
	public String getBasicPrem() {
		return basicPrem;
	}
	public void setBasicPrem(String basicPrem) {
		this.basicPrem = basicPrem;
	}
	public String getNettPrem() {
		return nettPrem;
	}
	public void setNettPrem(String nettPrem) {
		this.nettPrem = nettPrem;
	}
	public String getPremDue() {
		return premDue;
	}
	public void setPremDue(String premDue) {
		this.premDue = premDue;
	}
	public String getPremDueRounded() {
		return premDueRounded;
	}
	public void setPremDueRounded(String premDueRounded) {
		this.premDueRounded = premDueRounded;
	}
	public Risk getRisk() {
		return risk;
	}
	public void setRisk(Risk risk) {
		this.risk = risk;
	}
	public CoverNoteBenefits getCoverNoteBenefits() {
		return coverNoteBenefits;
	}
	public void setCoverNoteBenefits(CoverNoteBenefits coverNoteBenefits) {
		this.coverNoteBenefits = coverNoteBenefits;
	}
	public Payments getPayments() {
		return payments;
	}
	public void setPayments(Payments payments) {
		this.payments = payments;
	}
	public String getQuotationNo() {
		return quotationNo;
	}
	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}
	/**
	 * @return the isEPolicy
	 */
	public String getIsEPolicy() {
		return isEPolicy;
	}
	/**
	 * @param isEPolicy the isEPolicy to set
	 */
	public void setIsEPolicy(String isEPolicy) {
		this.isEPolicy = isEPolicy;
	}
	/**
	 * @return the cNoteStatus
	 */
	public String getcNoteStatus() {
		return cNoteStatus;
	}
	/**
	 * @param cNoteStatus the cNoteStatus to set
	 */
	public void setcNoteStatus(String cNoteStatus) {
		this.cNoteStatus = cNoteStatus;
	}

   

}
