
package com.sterw.roadwarrior.api;

import java.io.Serializable;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CoverNoteBenefit"
})
public class CoverNoteBenefits implements Serializable
{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("CoverNoteBenefit")
    private Set<CoverNoteBenefit> qtgcBenefits;
  
	public Set<CoverNoteBenefit> getQtgcBenefits() {
		return qtgcBenefits;
	}
	public void setQtgcBenefits(Set<CoverNoteBenefit> qtgcBenefits) {
		this.qtgcBenefits = qtgcBenefits;
	}
	
	
   

}
