
package com.sterw.roadwarrior.api;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CoverId",
    "FeeId",
    "FeeCode",
    "FeeAmt"
})
public class CoverFeesGrp implements Serializable
{
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("SubCoverId")
	private String subCoverNumber;
    @JsonProperty("CoverId")
    private String coverId;
    @JsonProperty("FeeId")
    private String feeId;
    @JsonProperty("FeeCode")
    private String feeCode;
    @JsonProperty("FeeAmt")
    private String feeAmt;
   
	public String getSubCoverNumber() {
		return subCoverNumber;
	}
	public void setSubCoverNumber(String subCoverNumber) {
		this.subCoverNumber = subCoverNumber;
	}
	public String getCoverId() {
		return coverId;
	}
	public void setCoverId(String coverId) {
		this.coverId = coverId;
	}
	public String getFeeId() {
		return feeId;
	}
	public void setFeeId(String feeId) {
		this.feeId = feeId;
	}
	public String getFeeCode() {
		return feeCode;
	}
	public void setFeeCode(String feeCode) {
		this.feeCode = feeCode;
	}
	public String getFeeAmt() {
		return feeAmt;
	}
	public void setFeeAmt(String feeAmt) {
		this.feeAmt = feeAmt;
	}
    
    

  
}
