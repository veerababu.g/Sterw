
package com.sterw.roadwarrior.api;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "HEADER",
    "DETAIL"
})
public class RetrieveQuotePaymentInfoDTO implements Serializable
{
	  
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("HEADER")
    private Header header;
    @JsonProperty("DETAIL")
    private Detail detail;
  
	public Header getHeader() {
		return header;
	}
	public void setHeader(Header header) {
		this.header = header;
	}
	public Detail getDetail() {
		return detail;
	}
	public void setDetail(Detail detail) {
		this.detail = detail;
	}

    

}
