
package com.sterw.roadwarrior.api;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CoverId",
    "BenefitCode",
    "BenefitPremium",
    "BenefitSumInsured"
})
public class CoverNoteBenefit implements Serializable
{
	 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("CoverId")
    private String coverId;
    @JsonProperty("BenefitCode")
    private String benefitCode;
    @JsonProperty("BenefitPremium")
    private String benefitPremium;
    @JsonProperty("BenefitSumInsured")
    private String benefitSumInsured;
    
    @JsonProperty("SubCoverId")
	private String subCoverNumber;
    
   
	public String getCoverId() {
		return coverId;
	}
	public void setCoverId(String coverId) {
		this.coverId = coverId;
	}
	public String getBenefitCode() {
		return benefitCode;
	}
	public void setBenefitCode(String benefitCode) {
		this.benefitCode = benefitCode;
	}
	public String getBenefitPremium() {
		return benefitPremium;
	}
	public void setBenefitPremium(String benefitPremium) {
		this.benefitPremium = benefitPremium;
	}
	public String getBenefitSumInsured() {
		return benefitSumInsured;
	}
	public void setBenefitSumInsured(String benefitSumInsured) {
		this.benefitSumInsured = benefitSumInsured;
	}
	public String getSubCoverNumber() {
		return subCoverNumber;
	}
	public void setSubCoverNumber(String subCoverNumber) {
		this.subCoverNumber = subCoverNumber;
	}
    
    

   

}
