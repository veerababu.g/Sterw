
package com.sterw.roadwarrior.api;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CoverFeesGrp"
})
public class CoverFees implements Serializable
{
	 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("CoverFeesGrp")
    private List<CoverFeesGrp> coverFeesGrp;
   
	public List<CoverFeesGrp> getCoverFeesGrp() {
		return coverFeesGrp;
	}
	public void setCoverFeesGrp(List<CoverFeesGrp> coverFeesGrp) {
		this.coverFeesGrp = coverFeesGrp;
	}

    
}
