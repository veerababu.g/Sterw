
package com.sterw.roadwarrior.api;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CoverId",
    "CoverCode",
    "Si",
    "BasicPrem",
    "AnnualPrem",
    "GrossPrem",
    "NettPrem",
    "PlanCode",
    "SubCover",
    "CoverFees"
})
public class CoverGrp implements Serializable
{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("CoverId")
    private String coverId;
    @JsonProperty("CoverCode")
    private String coverCode;
    @JsonProperty("Si")
    private String si;
    @JsonProperty("BasicPrem")
    private String basicPrem;
    @JsonProperty("AnnualPrem")
    private String annualPrem;
    @JsonProperty("GrossPrem")
    private String grossPrem;
    @JsonProperty("NettPrem")
    private String nettPrem;
    
    @JsonProperty("CommPct")
    private String commPct;
    
    @JsonProperty("CommAmt")
    private String commAmt;
    
    @JsonProperty("PlanCode")
    private String planCode;
    @JsonProperty("SubCover")
    private SubCover subCover;
    @JsonProperty("CoverFees")
    private CoverFees coverFees;
    
	public String getCoverId() {
		return coverId;
	}
	public void setCoverId(String coverId) {
		this.coverId = coverId;
	}
	public String getCoverCode() {
		return coverCode;
	}
	public void setCoverCode(String coverCode) {
		this.coverCode = coverCode;
	}
	public String getSi() {
		return si;
	}
	public void setSi(String si) {
		this.si = si;
	}
	public String getBasicPrem() {
		return basicPrem;
	}
	public void setBasicPrem(String basicPrem) {
		this.basicPrem = basicPrem;
	}
	public String getAnnualPrem() {
		return annualPrem;
	}
	public void setAnnualPrem(String annualPrem) {
		this.annualPrem = annualPrem;
	}
	public String getGrossPrem() {
		return grossPrem;
	}
	public void setGrossPrem(String grossPrem) {
		this.grossPrem = grossPrem;
	}
	public String getNettPrem() {
		return nettPrem;
	}
	public void setNettPrem(String nettPrem) {
		this.nettPrem = nettPrem;
	}
	public String getPlanCode() {
		return planCode;
	}
	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}
	public SubCover getSubCover() {
		return subCover;
	}
	public void setSubCover(SubCover subCover) {
		this.subCover = subCover;
	}
	public CoverFees getCoverFees() {
		return coverFees;
	}
	public void setCoverFees(CoverFees coverFees) {
		this.coverFees = coverFees;
	}
	public String getCommPct() {
		return commPct;
	}
	public void setCommPct(String commPct) {
		this.commPct = commPct;
	}
	public String getCommAmt() {
		return commAmt;
	}
	public void setCommAmt(String commAmt) {
		this.commAmt = commAmt;
	}

    

}
