package com.sterw.roadwarrior.util;

import com.sterw.roadwarrior.domain.model.dto.ResponseDTO;

/**
 * @author HCL
 *
 */
public class GenerateResponse {

	private GenerateResponse(){
		
	}
	
	/**
	 * This is used for set the result in the response DTO
	 * @param restult
	 * @param Status
	 * @return responseDTO
	 */

	public static <T> ResponseDTO<T> getSuccessResponse(Object  restult,String status){
		ResponseDTO responseDTO= new ResponseDTO<>();
		responseDTO.setStatus(status);
		responseDTO.setResult(restult);
		return responseDTO;
		
	}
	
}
