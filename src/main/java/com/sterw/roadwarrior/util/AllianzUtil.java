package com.sterw.roadwarrior.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.net.ssl.SSLContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.TrustStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sterw.roadwarrior.api.Cover;
import com.sterw.roadwarrior.api.CoverFees;
import com.sterw.roadwarrior.api.CoverFeesGrp;
import com.sterw.roadwarrior.api.CoverGrp;
import com.sterw.roadwarrior.api.CoverNoteBenefit;
import com.sterw.roadwarrior.api.CoverNoteBenefits;
import com.sterw.roadwarrior.api.Detail;
import com.sterw.roadwarrior.api.Header;
import com.sterw.roadwarrior.api.Payment;
import com.sterw.roadwarrior.api.Payments;
import com.sterw.roadwarrior.api.RetrieveQuotePaymentInfoDTO;
import com.sterw.roadwarrior.api.Risk;
import com.sterw.roadwarrior.api.RiskGrp;
import com.sterw.roadwarrior.api.RiskPerson;
import com.sterw.roadwarrior.api.RiskVeh;
import com.sterw.roadwarrior.api.SubCover;
import com.sterw.roadwarrior.api.SubCoverGrp;
import com.sterw.roadwarrior.constants.API;
import com.sterw.roadwarrior.constants.Constants;
import com.sterw.roadwarrior.domain.model.dto.BenefitDTO;
import com.sterw.roadwarrior.domain.model.dto.CoverDTO;
import com.sterw.roadwarrior.domain.model.dto.CoverFeeDTO;
import com.sterw.roadwarrior.domain.model.dto.GetApiSignatureRequestDTO;
import com.sterw.roadwarrior.domain.model.dto.PymtDTO;
import com.sterw.roadwarrior.domain.model.dto.RiskVehDTO;
import com.sterw.roadwarrior.domain.model.dto.SubCoverDTO;
import com.sterw.roadwarrior.domain.model.dto.TransactionDTO;

/**
 * @author HCL
 *
 */

@Component
public class AllianzUtil {
	private static final Logger logger = LogManager.getLogger(AllianzUtil.class);
	ModelMapper modelMapper=new ModelMapper();
	
  
    
   
    
    
    /**
     * This is used to get the API key based on quotation number 
     * @param quotation No- input as quotation number
     * @return API key
     */
    public String getAPIKey(String quotationNo){
    	 SimpleDateFormat df = new SimpleDateFormat(Constants.API_KEY_DATE_FORMAT);
    	return df.format(new Date())+API.TYPE.value()+quotationNo;
    }
    
    /**
     * This is used to get the upload time in the format yyyymmDDHHmmss  
     * @return date
     */
    public String getUploadTime(){
    	 SimpleDateFormat df = new SimpleDateFormat(Constants.API_UPLOADTIME_DATE_FORMAT);
     	return df.format(new Date());
    }
    
    
    /** 
     * This is used to get submit payment based on transactionDTO
     * @param transactionDTO
     * @return paymentInfoDTO
     */
    public RetrieveQuotePaymentInfoDTO getSubmitPaymentRequest(TransactionDTO transactionDTO){
    	RetrieveQuotePaymentInfoDTO paymentInfoDTO= new RetrieveQuotePaymentInfoDTO();
		
		paymentInfoDTO.setHeader(getPaymentSubmitHeader(transactionDTO.getQuotationNo()));
		paymentInfoDTO.setDetail(getPaymenSubmitDetail(transactionDTO));
		return paymentInfoDTO;
    }
    /**
     * This is used to get submit payment with in the header
     * @param quotationNo
     * @return header
     */
    public Header getPaymentSubmitHeader(String quotationNo){
    	Header header= new Header();
    	header.setKey(getAPIKey(quotationNo));
		header.setMethod(API.METHOD.value());
		header.setSourceSystem(API.SOURCESYSTEM.value());
		header.setUploadTime(getUploadTime());
		header.setType(API.TYPE.value());
		return header;
    }
    /**
     * This is used to get submit payment details based on transactionDTO
     * @param transactionDTO
     * @return payment submit detail
     */
    public Detail getPaymenSubmitDetail(TransactionDTO transactionDTO){
    	Detail detail;
    	
    	if(!StringUtils.isEmpty(transactionDTO.getGender()))
    		transactionDTO.setGender((transactionDTO.getGender().equalsIgnoreCase(Constants.MALE)?Constants.MALECODE:Constants.FEMALECODE));
		detail=modelMapper.map(transactionDTO, Detail.class);
    	detail.setCountryCode(API.COUNTRYCODE.value());
    	detail.setIsEPolicy(API.ISEPOLICY.value());
    	detail.setProductCode(API.PRODUCTCODE.value());
		detail.setProductType(API.PRODUCTTYPE.value());
		detail.setcNoteType(API.CNOTETYPE.value());
		detail.setcNoteStatus(API.CNOTESTATUS.value());
		detail.setInsuredName(detail.getClientName());
		if(!StringUtils.isEmpty(detail.getMobileNo())){
			String fullMobNo=detail.getMobileNo();
			detail.setMobileCode1(fullMobNo.substring(0, 4));
			detail.setMobileNo(fullMobNo.substring(4));
		}
		detail.setAddressType("R");
		CoverDTO coverDTO= new CoverDTO();
		if(!transactionDTO.getQtgcCovers().isEmpty())
			coverDTO= transactionDTO.getQtgcCovers().iterator().next();
		
		detail.setCoverNoteBenefits(setCoverNoteBenefits(coverDTO));
		detail.setPayments(setPayments(transactionDTO.getPymt()));
		detail.setRisk(setRisk(coverDTO,transactionDTO.getQtgcRiskVehs(),transactionDTO));
		
    	return detail;
    }
    
    /**
     * This is used to setting Cover Note benefit based on coverDTO
     * @param coverDTO
     * @return coverNoteBenefits
     */
    private CoverNoteBenefits setCoverNoteBenefits(CoverDTO coverDTO){
    	CoverNoteBenefits coverNoteBenefits= new CoverNoteBenefits();
    	Set<CoverNoteBenefit> coverNoteBenefit= new HashSet<>();
    	Set<BenefitDTO> benifitSet=coverDTO.getQtgcBenefits();
    	
    	for(BenefitDTO benefit:benifitSet){
			CoverNoteBenefit coverNB= new CoverNoteBenefit();
			coverNB.setBenefitCode(benefit.getBenefitCode());
			coverNB.setCoverId(null!=benefit.getCoverNO()?benefit.getCoverNO().toString():"");
			coverNB.setBenefitPremium(benefit.getBenefitPrem().toString());
			coverNB.setBenefitSumInsured(benefit.getBenefitSi().toString());
			coverNoteBenefit.add(coverNB);
			
		}
    	coverNoteBenefits.setQtgcBenefits(coverNoteBenefit);
    	return coverNoteBenefits;
    }
    
    /**
     * this is used to set payments based on pymt
     * @param pymt
     * @return payments
     */
    private Payments setPayments(PymtDTO  pymt){    	
    	Payments payments = new Payments();	
		Payment payment=new Payment();
		
		if(null!=pymt){
		payment.setPaymentAmount(null!=pymt.getPymtAmt()?pymt.getPymtAmt().toString():"");
		
		SimpleDateFormat df = new SimpleDateFormat(Constants.PAYMENT_DATE_FORMAT);
		
		payment.setPaymentDate(null!=pymt.getPymtDate()?df.format(pymt.getPymtDate()):"");
		payment.setPaymentId(pymt.getPymtNumber());
		payment.setSubmissionNo(pymt.getSubmissionNo());
		payment.setPaymentMode(API.PAYMENTMODE.value());
		payment.setPaymentBankRef(pymt.getPymtTxId());
		payments.setPayment(payment);
    }
    return payments;
    }
    
    /**
     * This is used to set risk based on coverDTO,getQtgcRiskVehs,transactionDTO
     * @param coverDTO
     * @param getQtgcRiskVehs
     * @param transactionDTO
     * @return risk
     */
    private Risk setRisk(CoverDTO coverDTO,RiskVehDTO getQtgcRiskVehs,TransactionDTO transactionDTO){
    	Risk risk= new Risk();
    	RiskGrp riskGrp= new RiskGrp();
    		setRiskGrupVeh(getQtgcRiskVehs,riskGrp);
    		setPerson(transactionDTO,riskGrp);
    		setCover(coverDTO,riskGrp);
    	risk.setRiskGrp(riskGrp);
    	return risk;
    }
    /**
     * This is used to set Cover based on coverDTO, riskGrp
     * @param coverDTO
     * @param riskGrp
     * @return void
     */
    private void setCover(CoverDTO coverDTO, RiskGrp riskGrp) {
    	Cover cover= new Cover();
    	setCoverGrp(cover,coverDTO);
    	riskGrp.setCover(cover);
		
	}
	/**
	 * this is used to set Cover Group based on cover, coverDTO
	 * @param cover
	 * @param coverDTO
	 * @return void
	 */
	private void setCoverGrp(Cover cover, CoverDTO coverDTO) {
		CoverGrp coverGrp= new CoverGrp();
		coverGrp.setAnnualPrem(null!=coverDTO.getCoverAnnualPrem()?coverDTO.getCoverAnnualPrem().toString():"");
		coverGrp.setBasicPrem(null!=coverDTO.getCoverBasicPrem()?coverDTO.getCoverBasicPrem().toString():"");
		coverGrp.setCoverCode(coverDTO.getCoverCode());
		coverGrp.setGrossPrem(null!=coverDTO.getCoverGrossPrem()?coverDTO.getCoverGrossPrem().toString():"");
		coverGrp.setNettPrem(null!=coverDTO.getCoverGrossPrem()?coverDTO.getCoverGrossPrem().toString():"");
		coverGrp.setSi(null!=coverDTO.getCoverSi()?coverDTO.getCoverSi().toString():"");
		
		coverGrp.setCoverId(null!=coverDTO.getCoverNumber()?coverDTO.getCoverNumber().toString():"");
		coverGrp.setPlanCode(coverDTO.getCoverPlanCode());
		coverGrp.setCommPct(null!=coverDTO.getRebatePct()?coverDTO.getRebatePct().toString():"");
		coverGrp.setCommAmt(null!=coverDTO.getRebateAmt()?coverDTO.getRebateAmt().toString():"");
		setSubCover(coverGrp,coverDTO);
		setCoverFee(coverGrp,coverDTO);
		cover.setCoverGrp(coverGrp);
		
	}
	/**
	 * This is used to set Cover fee based on coverGrp, coverDTO
	 * @param coverGrp
	 * @param coverDTO
	 * @return void
	 */
	private void setCoverFee(CoverGrp coverGrp, CoverDTO coverDTO) {
		CoverFees coverFees= new CoverFees();
		List<CoverFeesGrp> coverFeesGrp = new ArrayList<>();
		Set<CoverFeeDTO> coverFeeSet=coverDTO.getCoverFee();
		for(CoverFeeDTO coverFee:coverFeeSet){
			CoverFeesGrp coverFeesBean= new CoverFeesGrp();
			coverFeesBean.setCoverId(null!=coverFee.getCoverNumber()?coverFee.getCoverNumber().toString():"");
			coverFeesBean.setFeeAmt(null!=coverFee.getFeeAmount()?coverFee.getFeeAmount().toString():"");
			coverFeesBean.setFeeCode(coverFee.getFeeCode());
			coverFeesBean.setFeeId(coverFee.getFeeNumber());
			coverFeesGrp.add(coverFeesBean);
		}
		
		coverFees.setCoverFeesGrp(coverFeesGrp);
		coverGrp.setCoverFees(coverFees);
		
	}
	/**
	 * This is used to set Sub Cover  based on coverGrp, coverDTO
	 * @param coverGrp
	 * @param coverDTO
	 * @return void
	 */
	private void setSubCover(CoverGrp coverGrp, CoverDTO coverDTO) {
		SubCover subCover = new SubCover();
		Set<SubCoverGrp> subCoverGrp = new HashSet<>();
		Set<SubCoverDTO> subCoverSet=coverDTO.getSubCover();
		for(SubCoverDTO sc:subCoverSet){
			SubCoverGrp subCoverBean= new SubCoverGrp();
			subCoverBean.setSubCoverId(null!=sc.getSubCoverNumber()?sc.getSubCoverNumber().toString():"");
			subCoverBean. setPlanCode(sc.getPlanCode());
			subCoverBean.setSubCoverCode(sc.getSubCoverCode());
			subCoverBean.setSubCoverId(null!=sc.getSubCoverNumber()?sc.getSubCoverNumber().toString():"");
			subCoverBean.setSubPrem(null!=sc.getSubCoverNumber()?sc.getSubPremium().toString():"");
			subCoverBean.setSubSi(null!=sc.getSubSi()?sc.getSubSi().toString():"");
			subCoverGrp.add(subCoverBean);
		}
		if(!subCoverGrp.isEmpty() ){
			subCover.setSubCoverGrp(subCoverGrp);
		}
		coverGrp.setSubCover(subCover);
		
	}
	/**
	 * This is used to create RiskPerson object based on transactionDTO, riskGrp
	 * @param transactionDTO
	 * @param riskGrp
	 * @return void
	 */
	private void setPerson(TransactionDTO transactionDTO, RiskGrp riskGrp) {
    	RiskPerson riskPerson = new RiskPerson();
    	
		riskPerson.setGender(transactionDTO.getGender());
		riskPerson.setName(transactionDTO.getClientName());
		riskPerson.setIdType1(transactionDTO.getClientType());
		riskPerson.setIdValue1(transactionDTO.getClientNumber());
		riskPerson.setOccupation(transactionDTO.getOccupationCode());
		riskPerson.setOccupationDescription(transactionDTO.getOccupation());
		riskGrp.setRiskPerson(riskPerson);
		
	}
	/**
	 * This is used to set Risk Group Vehicle based on riskVehDto, riskGrp
	 * @param riskVehDto
	 * @param riskGrp
	 * @return void
	 */
	private void setRiskGrupVeh(RiskVehDTO riskVehDto,RiskGrp riskGrp){
    	RiskVeh riskVeh= new RiskVeh();
    	riskGrp.setRiskId(null!=riskVehDto.getRiskId()?riskVehDto.getRiskId().toString():"");
		riskGrp.setRiskLocated(API.RISKLOCATED.value());
		riskGrp.setRiskType(API.RISKTYPE.value());
		riskVeh.setVehicleSeat(null!=riskVehDto.getSeatCapacity()?riskVehDto.getSeatCapacity().toString():"");
		riskVeh.setVehicleNo(riskVehDto.getVehNo());
		riskVeh.setVehicleModel(riskVehDto.getVehModel());
		riskGrp.setRiskVehs(riskVeh);
    }

	
	/**
	 * This is used to invoke the API for getting submit payment based on retrieveQuotePaymentInfo, apiToken, submitPaymentAPIURL
	 * @param retrieveQuotePaymentInfo
	 * @param apiToken
	 * @param submitPaymentAPIURL
	 * @return ResponseEntity 
	 * @throws KeyManagementException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyStoreException
	 */
public  ResponseEntity<String>  invokeSubmitPaymentAPI(RetrieveQuotePaymentInfoDTO retrieveQuotePaymentInfo, String apiToken, String submitPaymentAPIURL) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException{
		
		try{
			logger.info("****************ApiToken***************"+apiToken);
			logger.info("*****************ApiURL*********"+submitPaymentAPIURL);
			RestTemplate restTemplate = restTemplate();
			ObjectMapper mapper= new ObjectMapper();
			 restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
			HttpHeaders headers = new HttpHeaders();
			headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
			headers.add("Authorization", "Bearer " +apiToken); 
		   
			HttpEntity<String> request = new HttpEntity<>(mapper.writeValueAsString(retrieveQuotePaymentInfo), headers);
			ResponseEntity<String> response = restTemplate
			            .exchange(submitPaymentAPIURL, HttpMethod.POST, request, String.class);
			logger.info("*****************ApiResponse*********"+response);
			return response;
			}catch(Exception e){
				logger.error("Customer invokeSubmitPaymentAPI  faild: {}",e);
				return new ResponseEntity<>(Constants.FAIL,HttpStatus.INTERNAL_SERVER_ERROR);
			}
    }


/**
 * This will provide rest template with SSL connectivity
 * @return RestTemplate
 * @throws KeyStoreException
 * @throws NoSuchAlgorithmException
 * @throws KeyManagementException
 */
public RestTemplate restTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
TrustStrategy acceptingTrustStrategy = new TrustStrategy() {
       @Override public boolean isTrusted(X509Certificate[] x509Certificates, String s)
                       throws CertificateException {
           return true;
       }
   };

   SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                   .loadTrustMaterial(null, acceptingTrustStrategy)
                   .build();

   SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

   CloseableHttpClient httpClient = HttpClients.custom()
                   .setSSLSocketFactory(csf)
                   .build();

   HttpComponentsClientHttpRequestFactory requestFactory =
                   new HttpComponentsClientHttpRequestFactory();

   requestFactory.setHttpClient(httpClient);
   return new RestTemplate(requestFactory);
   
}

/**
 * This is used to get payment from httpRequest  
 * @param request
 * @return pymtDTO 
 */
public PymtDTO getPymtFromHttpRequest(HttpServletRequest request){
	
	PymtDTO pymtDTO= new PymtDTO();
	if(null!=request.getParameter("Amount"))
		pymtDTO.setPymtAmt(getParseBigDecimal(request.getParameter("Amount")));
	pymtDTO.setMerchantCode(request.getParameter("MerchantCode"));
	pymtDTO.setPymtApprCode(request.getParameter("AuthCode"));
	pymtDTO.setPymtDate(new Timestamp(new Date().getTime()));
	pymtDTO.setPymtNumber(request.getParameter("PaymentId"));
	pymtDTO.setPymtStatus(request.getParameter("Status"));
	pymtDTO.setPymtTxId(request.getParameter("TransId"));
	pymtDTO.setSubmissionNo(request.getParameter("RefNo"));
	pymtDTO.setErrDesc(request.getParameter("ErrDesc"));
	pymtDTO.setSignature(request.getParameter("Signature"));
	return pymtDTO;
}

/**
 * This is used to parse the BigDecimal values based on decimalValue
 * @param decimalValue
 * @return bigDecimal
 */
public BigDecimal getParseBigDecimal(String decimalValue){
	DecimalFormatSymbols symbols = new DecimalFormatSymbols();
	symbols.setGroupingSeparator(',');
	symbols.setDecimalSeparator('.');
	String pattern = "#,##0.0#";
	DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
	decimalFormat.setParseBigDecimal(true);

	// parse the string
	BigDecimal bigDecimal;
	try {
		bigDecimal = (BigDecimal) decimalFormat.parse(decimalValue);
		return bigDecimal;
	} catch (ParseException e) {
		return null;
	}
	
}
/**
 * This is used to get modified date of customer based on creation date
 * @param creationDate
 * @return Date
 * @throws ParseException
 */
public Date getModifiedDate(String creationDate) throws ParseException  {
    
SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
Date dt = format.parse(creationDate);
Date newCreationDate = new Date(dt.getTime());            
Calendar cal = Calendar.getInstance();
cal.setTime(newCreationDate);
cal.add(Calendar.MINUTE, 30);

return new Date(cal.getTimeInMillis());
}

/**
 * This method is used for get quotation list with comma separator
 * @param quotationList
 * @return list of quotation
 */
public String getQuotationsByCommaSeperator(List<String> quotationList){
    
    List<String> newQuotationList = new ArrayList<>();
  for(String quotation : quotationList){
    newQuotationList.add("'"+quotation+"'");
  } 
  return StringUtils.collectionToCommaDelimitedString(newQuotationList);

}

public boolean checkPaymentEnquiry(PymtDTO pymtDTO,String ipayEnquiryURI,GetApiSignatureRequestDTO getApiSignatureRequestDTO)  {
	try{
	RestTemplate restTemplate = restTemplate();
	HttpHeaders headers = new HttpHeaders();
	headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
	MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
	map.add("MerchantCode", pymtDTO.getMerchantCode());
	map.add("RefNo", pymtDTO.getSubmissionNo());
	map.add("Amount",(null!=pymtDTO.getPymtAmt())? pymtDTO.getPymtAmt().toString():"");
	HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
	ResponseEntity<String> response = restTemplate.postForEntity( ipayEnquiryURI, request , String.class );
	logger.info("*****************Ipay 88 checkPaymentEnquiry Response*********"+response.getBody());
	logger.info("*****************Ipay 88 payment Signature*********"+pymtDTO.getSignature());
	logger.info("*****************Genarated Signature*********"+getApiSignatureRequestDTO.getApiSignature());
	
	if((response.getBody().equalsIgnoreCase(Constants.PAYMENTSUCCESSFUL)||
			response.getBody().equalsIgnoreCase(Constants.PAYMENTFAIL)) 
			&& pymtDTO.getSignature().equalsIgnoreCase(getApiSignatureRequestDTO.getApiSignature())){
			return true;
	}
	
	}catch(Exception e){
		logger.error("Ipay 88 checkPaymentEnquiry   faild: {}",e);
		return false;
	}
	return false;
}

public PrivateKey readPrivateKey(String privateKey)
		throws IOException, GeneralSecurityException {
	PrivateKey key;
	byte[] keyBytes = DatatypeConverter.parseBase64Binary(privateKey);
	PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
	KeyFactory kf = KeyFactory.getInstance("RSA");
	key = kf.generatePrivate(spec);
	return key;
}



}
