package com.sterw.roadwarrior.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StringUtils;

public class IPWhiteListFilter implements  Filter{
	 ServletContext context;
	 
	 private FilterConfig filterConfig = null;
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig=filterConfig;
		this.context = filterConfig.getServletContext();
		this.context.log("IPWhiteListFilter initialized");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		String xforwarded = httpRequest.getHeader("X-Forwarded-For");
		this.context.log("X-Forwarded-For"+xforwarded);
		if(!StringUtils.isEmpty(xforwarded)&& xforwarded.split(",")[0].equalsIgnoreCase(filterConfig.getInitParameter("x-forward-ip")))
			 chain.doFilter(httpRequest, httpResponse);
		else{
			httpResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
			
		}
	}

	@Override
	public void destroy() {
		this.context.log("IPWhiteListFilter destroy");
	}

	

}
