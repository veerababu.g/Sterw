package com.sterw.roadwarrior.domain.model.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author HCL
 *
 */
public class DuplicateCustomerResponseDTO implements Serializable {
	
	private static final long serialVersionUID = 7558987569263922904L;
		@JsonProperty("IsCustomerDuplicate")
		private String isCustomerDuplicate;
		/**
		 * @param isCustomerDuplicate the isCustomerDuplicate to set
		 */
		public void setIsCustomerDuplicate(String isCustomerDuplicate) {
			this.isCustomerDuplicate = isCustomerDuplicate;
		}

}