package com.sterw.roadwarrior.domain.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the "QTGC_COVER_FEE" database table.
 * 
 */
@Entity
@Table(name="QTGC_COVER_FEE")
public class CoverFee implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="FEE_ID")
	private Integer feeNumber;
	@Column(name="FEE_AMOUNT")
	private BigDecimal feeAmount;
	
	@Column(name="COVER_ID",insertable=false,updatable=false)
	private Integer coverNumber;

	public Integer getCoverNumber() {
		return coverNumber;
	}
	public void setCoverNumber(Integer coverNumber) {
		this.coverNumber = coverNumber;
	}
	@Column(name="FEE_CODE")
	private String feeCode;

	@Column(name="SUB_COVER_ID",insertable=false,updatable=false)
	private Integer subCoverNumber;

	
	@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.REFRESH)
	@JoinColumns({
		@JoinColumn(name="COVER_ID",referencedColumnName="COVER_ID")	
	})
	private Cover qtgcCover;
	

	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumns({
		@JoinColumn(name="SUB_COVER_ID",referencedColumnName="SUB_COVER_ID")
	})
	private SubCover qtgcSubCover;
	public SubCover getQtgcSubCover() {
		return qtgcSubCover;
	}
	public void setQtgcSubCover(SubCover qtgcSubCover) {
		this.qtgcSubCover = qtgcSubCover;
	}
	

	public BigDecimal getFeeAmount() {
		return this.feeAmount;
	}

	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}

	public String getFeeCode() {
		return this.feeCode;
	}

	public void setFeeCode(String feeCode) {
		this.feeCode = feeCode;
	}


	public Cover getQtgcCover() {
		return qtgcCover;
	}



	public void setQtgcCover(Cover qtgcCover) {
		this.qtgcCover = qtgcCover;
	}



	public Integer getFeeNumber() {
		return feeNumber;
	}



	public void setFeeNumber(Integer feeNumber) {
		this.feeNumber = feeNumber;
	}
	public Integer getSubCoverNumber() {
		return subCoverNumber;
	}
	public void setSubCoverNumber(Integer subCoverNumber) {
		this.subCoverNumber = subCoverNumber;
	}



}