package com.sterw.roadwarrior.domain.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
@Embeddable
public class PaymtPK implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name="SUBMISSION_NO",insertable=false,updatable=false)
	private String submissionNo;
	@Column(name="QUOTATION_NO")
	private String quotationNo;
	public String getSubmissionNo() {
		return submissionNo;
	}
	public void setSubmissionNo(String submissionNo) {
		this.submissionNo = submissionNo;
	}
	public String getQuotationNo() {
		return quotationNo;
	}
	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((quotationNo == null) ? 0 : quotationNo.hashCode());
		result = prime * result + ((submissionNo == null) ? 0 : submissionNo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymtPK other = (PaymtPK) obj;
		if (quotationNo == null) {
			if (other.quotationNo != null)
				return false;
		} else if (!quotationNo.equals(other.quotationNo))
			return false;
		if (submissionNo == null) {
			if (other.submissionNo != null)
				return false;
		} else if (!submissionNo.equals(other.submissionNo))
			return false;
		return true;
	}
	
}
