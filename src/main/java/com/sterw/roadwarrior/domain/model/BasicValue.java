package com.sterw.roadwarrior.domain.model;

import java.io.Serializable;
import javax.persistence.*;



/**
 * @author HCL
 *
 */
@Entity
@Table(name="QTGC_BASIC_VALUES")

public class BasicValue implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID")
	private Integer basicId;
	@Column(name="BASIC_VALUES")
	private String values;
	@Column(name="VALUES_CATEGORY")
	private String valuesCategory;
	@Column(name="QUOTATION_NO")
	private String quotationNo;
	
	public String getQuotationNo() {
		return quotationNo;
	}
	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumns({
		@JoinColumn(name="QUOTATION_NO" ,referencedColumnName="QUOTATION_NO",insertable=false,updatable=false)
		})
	private Transaction qtgcTransaction;

	
	public String getValues() {
		return this.values;
	}

	public void setValues(String values) {
		this.values = values;
	}

	public String getValuesCategory() {
		return this.valuesCategory;
	}

	public void setValuesCategory(String valuesCategory) {
		this.valuesCategory = valuesCategory;
	}

	public Transaction getQtgcTransaction() {
		return this.qtgcTransaction;
	}

	public void setQtgcTransaction(Transaction qtgcTransaction) {
		this.qtgcTransaction = qtgcTransaction;
	}
	public Integer getBasicId() {
		return basicId;
	}
	public void setBasicId(Integer basicId) {
		this.basicId = basicId;
	}
	
	

}