package com.sterw.roadwarrior.domain.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the "QTGC_SUB_COVER" database table.
 * 
 */
@Entity
@Table(name="QTGC_SUB_COVER")
public class SubCover implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="SUB_COVER_ID")
	private Integer subCoverNumber;
	@Column(name="PLAN_CODE")
	private String planCode;
	@Column(name="COVER_ID",insertable=false,updatable=false)
	private Integer coverNumber;
	
	@Column(name="SUB_COVER_CODE")
	private String subCoverCode;
	

	@Column(name="SUB_PREMIUM")
	private BigDecimal subPremium;

	@Column(name="SUB_SI")
	private BigDecimal subSi;

	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="COVER_ID",referencedColumnName="COVER_ID")
		})
	private Cover qtgcCoverSub;
	
	@OneToMany(targetEntity=CoverFee.class, fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumns({
		@JoinColumn(name="COVER_ID",referencedColumnName="COVER_ID"),
		@JoinColumn(name="SUB_COVER_ID",referencedColumnName="SUB_COVER_ID")
		
	})
	private Set<CoverFee> subCoverFees;
	
	@OneToMany(targetEntity=Benefit.class, fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumns({
		@JoinColumn(name="COVER_ID",referencedColumnName="COVER_ID"),
		@JoinColumn(name="SUB_COVER_ID",referencedColumnName="SUB_COVER_ID")
		
	})
	private Set<Benefit> subCoverBenifit;

	
	


	

	



	public Integer getCoverNumber() {
		return coverNumber;
	}





	public void setCoverNumber(Integer coverNumber) {
		this.coverNumber = coverNumber;
	}





	public String getPlanCode() {
		return this.planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	public String getSubCoverCode() {
		return this.subCoverCode;
	}

	public void setSubCoverCode(String subCoverCode) {
		this.subCoverCode = subCoverCode;
	}

	


	


	public Integer getSubCoverNumber() {
		return subCoverNumber;
	}



	public void setSubCoverNumber(Integer subCoverNumber) {
		this.subCoverNumber = subCoverNumber;
	}



	public BigDecimal getSubPremium() {
		return this.subPremium;
	}

	public void setSubPremium(BigDecimal subPremium) {
		this.subPremium = subPremium;
	}

	public BigDecimal getSubSi() {
		return this.subSi;
	}

	public void setSubSi(BigDecimal subSi) {
		this.subSi = subSi;
	}



	public Cover getQtgcCoverSub() {
		return qtgcCoverSub;
	}



	public void setQtgcCoverSub(Cover qtgcCoverSub) {
		this.qtgcCoverSub = qtgcCoverSub;
	}



	public Set<CoverFee> getSubCoverFees() {
		return subCoverFees;
	}



	public void setSubCoverFees(Set<CoverFee> subCoverFees) {
		this.subCoverFees = subCoverFees;
	}



	public Set<Benefit> getSubCoverBenifit() {
		return subCoverBenifit;
	}



	public void setSubCoverBenifit(Set<Benefit> subCoverBenifit) {
		this.subCoverBenifit = subCoverBenifit;
	}







	
	

	

	

}