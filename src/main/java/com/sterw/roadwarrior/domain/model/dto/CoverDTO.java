package com.sterw.roadwarrior.domain.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * @author HCL
 *
 */
@JsonInclude(Include.NON_NULL)
public class CoverDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("AnnualPrem")
	private BigDecimal coverAnnualPrem;
	private String quotationNo;
	@JsonProperty("CoverId")
	private Integer coverNumber;
	@Column(name="RiskId")
	private String riskNo="";
	
	@JsonProperty("BasicPrem")
	private BigDecimal coverBasicPrem;
	@JsonProperty("GrossPrem")
	private BigDecimal coverGrossPrem;
	
	@JsonProperty("NettPrem")
	private BigDecimal coverNettPrem;
	
	@JsonProperty("CoverPlanCode")
	private String coverPlanCode;
	@JsonProperty("CoverCode")
	private String coverCode;
	
	
	@JsonProperty("CommAmt")
	private BigDecimal commissionAmt;
	
	@JsonProperty("CommPct")
	private Integer commisionPct;
	@JsonProperty("CoverSI")
	private BigDecimal coverSi;
	
	@JsonProperty("RebatePercentage")
	private Integer rebatePct;
	@JsonProperty("RebateAmount")
	private BigDecimal rebateAmt;
	
	@JsonProperty("BenefitList")
	private Set<BenefitDTO> qtgcBenefits;
	
	@JsonProperty("SubCoverList")
	private Set<SubCoverDTO> subCover;
	@JsonProperty("CoverFeeList")
	private Set<CoverFeeDTO> coverFee;
	
	public BigDecimal getCoverAnnualPrem() {
		return coverAnnualPrem;
	}
	public void setCoverAnnualPrem(BigDecimal coverAnnualPrem) {
		this.coverAnnualPrem = coverAnnualPrem;
	}
	public BigDecimal getCoverBasicPrem() {
		return coverBasicPrem;
	}
	public void setCoverBasicPrem(BigDecimal coverBasicPrem) {
		this.coverBasicPrem = coverBasicPrem;
	}
	public BigDecimal getCoverGrossPrem() {
		return coverGrossPrem;
	}
	public void setCoverGrossPrem(BigDecimal coverGrossPrem) {
		this.coverGrossPrem = coverGrossPrem;
	}
	public BigDecimal getCoverNettPrem() {
		return coverNettPrem;
	}
	public void setCoverNettPrem(BigDecimal coverNettPrem) {
		this.coverNettPrem = coverNettPrem;
	}
	public String getCoverPlanCode() {
		return coverPlanCode;
	}
	public void setCoverPlanCode(String coverPlanCode) {
		this.coverPlanCode = coverPlanCode;
	}
	public BigDecimal getCoverSi() {
		return coverSi;
	}
	public void setCoverSi(BigDecimal coverSi) {
		this.coverSi = coverSi;
	}
	public Set<BenefitDTO> getQtgcBenefits() {
		return qtgcBenefits;
	}
	public void setQtgcBenefits(Set<BenefitDTO> qtgcBenefits) {
		this.qtgcBenefits = qtgcBenefits;
	}
	public Set<SubCoverDTO> getSubCover() {
		return subCover;
	}
	public void setSubCover(Set<SubCoverDTO> subCover) {
		this.subCover = subCover;
	}
	public Set<CoverFeeDTO> getCoverFee() {
		return coverFee;
	}
	public void setCoverFee(Set<CoverFeeDTO> coverFee) {
		this.coverFee = coverFee;
	}
	public Integer getCoverNumber() {
		return coverNumber;
	}
	public void setCoverNumber(Integer coverNumber) {
		this.coverNumber = coverNumber;
	}
	public Integer getRebatePct() {
		return rebatePct;
	}
	public void setRebatePct(Integer rebatePct) {
		this.rebatePct = rebatePct;
	}
	public BigDecimal getRebateAmt() {
		return rebateAmt;
	}
	public void setRebateAmt(BigDecimal rebateAmt) {
		this.rebateAmt = rebateAmt;
	}
	public BigDecimal getCommissionAmt() {
		return commissionAmt;
	}
	public void setCommissionAmt(BigDecimal commissionAmt) {
		this.commissionAmt = commissionAmt;
	}
	public Integer getCommisionPct() {
		return commisionPct;
	}
	public void setCommisionPct(Integer commisionPct) {
		this.commisionPct = commisionPct;
	}
	public String getRiskNo() {
		return riskNo;
	}
	public void setRiskNo(String riskNo) {
		this.riskNo = riskNo;
	}
	public String getQuotationNo() {
		return quotationNo;
	}
	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}
	

	/**
	 * @return the coverCode
	 */
	public String getCoverCode() {
		return coverCode;
	}
	/**
	 * @param coverCode the coverCode to set
	 */
	public void setCoverCode(String coverCode) {
		this.coverCode = coverCode;
	}

	
}
