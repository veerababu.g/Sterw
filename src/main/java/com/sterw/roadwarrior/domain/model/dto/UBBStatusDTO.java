package com.sterw.roadwarrior.domain.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author HCL
 *
 */
public class UBBStatusDTO {
	@JsonProperty("QuotationNumber")
	private String quotationNo;
	@JsonProperty("CheckUBBStatus")
	private String checkUBBStatus;
	
	@JsonProperty("ReferCode")
	private String referCode;
	
	public String getQuotationNo() {
		return quotationNo;
	}
	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}
	public String getCheckUBBStatus() {
		return checkUBBStatus;
	}
	public void setCheckUBBStatus(String checkUBBStatus) {
		this.checkUBBStatus = checkUBBStatus;
	}
	public String getReferCode() {
		return referCode;
	}
	public void setReferCode(String referCode) {
		this.referCode = referCode;
	}
	
}
