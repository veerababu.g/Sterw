package com.sterw.roadwarrior.domain.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the "QTGC_TRANSACTION" database table.
 * 
 */
@Entity
@Table(name="QTGC_TRANSACTION")

public class Transaction implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="QUOTATION_NO")
	private String quotationNo;

	@Column(name="ADDRESS_1")
	private String address1;

	@Column(name="ADDRESS_2")
	private String address2;

	@Column(name="ADDRESS_3")
	private String address3;

	@Column(name="AGENT_CODE")
	private String agentCode;

	@Column(name="ANNUAL_PREM")
	private BigDecimal annualPrem;

	@Column(name="BASIC_PREM")
	private BigDecimal basicPrem;

	@Column(name="BRANCH")
	private String branch;

	@Column(name="CITY")
	private String city;

	@Column(name="CLIENT_ID")
	private String clientNumber;

	@Column(name="CLIENT_ID_TYPE")
	private String clientType;

	@Column(name="CLIENT_NAME")
	private String clientName;

	@Column(name="COMMISSION_AMT")
	private BigDecimal commissionAmt;

	@Temporal(TemporalType.DATE)
	@Column(name="COMP_REG_DATE")
	private Date compRegDate;

	@Column(name="COUNTRY")
	private String country;

    @Column(name="CREATED_DT")
    private Timestamp createdDt;


	@Temporal(TemporalType.DATE)
	@Column(name="DOB")
	private Date dob;

	@Temporal(TemporalType.DATE)
	@Column(name="EFF_DATE")
	private Date effDate;

	@Column(name="EMAIL")
	private String email;

	@Temporal(TemporalType.DATE)
	@Column(name="EXP_DATE")
	private Date expDate;

	@Column(name="GENDER")
	private String gender;

	@Column(name="GROSS_PREM")
	private BigDecimal grossPrem;

	@Column(name="GST_AMT")
	private BigDecimal gstAmt;

	@Column(name="GST_PCT")
	private BigDecimal gstPct;

	@Column(name="MOBILE_NO")
	private BigDecimal mobileNo;

	@Column(name="NETT_PREM")
	private BigDecimal nettPrem;

	@Column(name="OCCUPATION")
	private String occupation;
	
	@Column(name="OCCUP_CODE")
	private String occupationCode;
	
	@Column(name="POLICY_SI")
	private BigDecimal policySi;

	@Column(name="POSTCODE")
	private Integer postcode;

	@Column(name="PREM_DUE")
	private BigDecimal premDue;

	@Column(name="PREM_DUE_ROUNDED")
	private BigDecimal premDueRounded;

	@Column(name="PRODUCT_CODE")
	private String productCode;

	@Column(name="PRODUCT_SHORT_DESC")
	private String productShortDesc;

	@Column(name="QUOTATION_PROGRESS")
	private String quotationProgress;

	@Column(name="REBATE_AMT")
	private BigDecimal rebateAmt;

	@Column(name="REBATE_PCT")
	private BigDecimal rebatePct;

	@Column(name="STAMP")
	private BigDecimal stamp;

	@Column(name="STATE")
	private String state;

	@Column(name="UBB_STATUS")
	private String ubbStatus;
	
	@OneToMany(targetEntity=BasicValue.class, fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="QUOTATION_NO",referencedColumnName="QUOTATION_NO")
	
	private Set<BasicValue> qtgcBasicValues;
	
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "vehTransaction",fetch=FetchType.EAGER)
	private RiskVeh qtgcRiskVehs;
	
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "policyTransaction")
	private Policy policy;
	
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "pymtTransaction",fetch=FetchType.EAGER)
	private Pymt pymt;
	
	@OneToMany(targetEntity=Cover.class, fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="QUOTATION_NO",referencedColumnName="QUOTATION_NO")
	
	private Set<Cover> qtgcCovers;
	public String getQuotationNo() {
		return quotationNo;
	}

	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public BigDecimal getAnnualPrem() {
		return annualPrem;
	}

	public void setAnnualPrem(BigDecimal annualPrem) {
		this.annualPrem = annualPrem;
	}

	public BigDecimal getBasicPrem() {
		return basicPrem;
	}

	public void setBasicPrem(BigDecimal basicPrem) {
		this.basicPrem = basicPrem;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getClientNumber() {
		return clientNumber;
	}

	public void setClientNumber(String clientNumber) {
		this.clientNumber = clientNumber;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public BigDecimal getCommissionAmt() {
		return commissionAmt;
	}

	public void setCommissionAmt(BigDecimal commissionAmt) {
		this.commissionAmt = commissionAmt;
	}

	public Date getCompRegDate() {
		return compRegDate;
	}

	public void setCompRegDate(Date compRegDate) {
		this.compRegDate = compRegDate;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	

	/**
	 * @return the createdDt
	 */
	public Timestamp getCreatedDt() {
		return createdDt;
	}

	/**
	 * @param createdDt the createdDt to set
	 */
	public void setCreatedDt(Timestamp createdDt) {
		this.createdDt = createdDt;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Date getEffDate() {
		return effDate;
	}

	public void setEffDate(Date effDate) {
		this.effDate = effDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getExpDate() {
		return expDate;
	}

	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public BigDecimal getGrossPrem() {
		return grossPrem;
	}

	public void setGrossPrem(BigDecimal grossPrem) {
		this.grossPrem = grossPrem;
	}

	public BigDecimal getGstAmt() {
		return gstAmt;
	}

	public void setGstAmt(BigDecimal gstAmt) {
		this.gstAmt = gstAmt;
	}

	

	

	public BigDecimal getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(BigDecimal mobileNo) {
		this.mobileNo = mobileNo;
	}

	public BigDecimal getNettPrem() {
		return nettPrem;
	}

	public void setNettPrem(BigDecimal nettPrem) {
		this.nettPrem = nettPrem;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public BigDecimal getPolicySi() {
		return policySi;
	}

	public void setPolicySi(BigDecimal policySi) {
		this.policySi = policySi;
	}

	public Integer getPostcode() {
		return postcode;
	}

	public void setPostcode(Integer postcode) {
		this.postcode = postcode;
	}

	public BigDecimal getPremDue() {
		return premDue;
	}

	public void setPremDue(BigDecimal premDue) {
		this.premDue = premDue;
	}

	

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductShortDesc() {
		return productShortDesc;
	}

	public void setProductShortDesc(String productShortDesc) {
		this.productShortDesc = productShortDesc;
	}

	public String getQuotationProgress() {
		return quotationProgress;
	}

	public void setQuotationProgress(String quotationProgress) {
		this.quotationProgress = quotationProgress;
	}

	public BigDecimal getRebateAmt() {
		return rebateAmt;
	}

	public void setRebateAmt(BigDecimal rebateAmt) {
		this.rebateAmt = rebateAmt;
	}
	public BigDecimal getGstPct() {
		return gstPct;
	}

	public void setGstPct(BigDecimal gstPct) {
		this.gstPct = gstPct;
	}

	public BigDecimal getPremDueRounded() {
		return premDueRounded;
	}

	public void setPremDueRounded(BigDecimal premDueRounded) {
		this.premDueRounded = premDueRounded;
	}

	public BigDecimal getRebatePct() {
		return rebatePct;
	}

	public void setRebatePct(BigDecimal rebatePct) {
		this.rebatePct = rebatePct;
	}

	public BigDecimal getStamp() {
		return stamp;
	}

	public void setStamp(BigDecimal stamp) {
		this.stamp = stamp;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getUbbStatus() {
		return ubbStatus;
	}

	public void setUbbStatus(String ubbStatus) {
		this.ubbStatus = ubbStatus;
	}

	public Set<BasicValue> getQtgcBasicValues() {
		return qtgcBasicValues;
	}

	public void setQtgcBasicValues(Set<BasicValue> qtgcBasicValues) {
		this.qtgcBasicValues = qtgcBasicValues;
	}
	public RiskVeh getQtgcRiskVehs() {
		return qtgcRiskVehs;
	}

	public void setQtgcRiskVehs(RiskVeh qtgcRiskVehs) {
		this.qtgcRiskVehs = qtgcRiskVehs;
	}

	public Set<Cover> getQtgcCovers() {
		return qtgcCovers;
	}

	public void setQtgcCovers(Set<Cover> qtgcCovers) {
		this.qtgcCovers = qtgcCovers;
	}

	public Policy getPolicy() {
		return policy;
	}

	public void setPolicy(Policy policy) {
		this.policy = policy;
	}

	public Pymt getPymt() {
		return pymt;
	}

	public void setPymt(Pymt pymt) {
		this.pymt = pymt;
	}

	public String getOccupationCode() {
		return occupationCode;
	}

	public void setOccupationCode(String occupationCode) {
		this.occupationCode = occupationCode;
	}
	
	
	
}