package com.sterw.roadwarrior.domain.model.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author HCL
 *
 */
public class QuotationResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("QuotationNumber")
	private String quotationNumber;
	@JsonProperty("RiskId")
	private Integer riskId;
	@JsonProperty("CoverId")
	private List<Integer> coverId;
	@JsonProperty("SubCoverId")
	private List<Integer> subCoverId;
	@JsonProperty("FeeId")
	private List<Integer> feeId;
	public String getQuotationNumber() {
		return quotationNumber;
	}
	public void setQuotationNumber(String quotationNumber) {
		this.quotationNumber = quotationNumber;
	}
	public Integer getRiskId() {
		return riskId;
	}
	public void setRiskId(Integer riskId) {
		this.riskId = riskId;
	}
	public List<Integer> getCoverId() {
		return coverId;
	}
	public void setCoverId(List<Integer> coverId) {
		this.coverId = coverId;
	}
	public List<Integer> getSubCoverId() {
		return subCoverId;
	}
	public void setSubCoverId(List<Integer> subCoverId) {
		this.subCoverId = subCoverId;
	}
	public List<Integer> getFeeId() {
		return feeId;
	}
	public void setFeeId(List<Integer> feeId) {
		this.feeId = feeId;
	}
	
	
	
	
	
	
	
	
}
