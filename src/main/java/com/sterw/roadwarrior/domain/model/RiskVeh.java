package com.sterw.roadwarrior.domain.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the "QTGC_RISK_VEH" database table.
 * 
 */
@Entity
@Table(name="QTGC_RISK_VEH")
public class RiskVeh implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="RISK_ID")
	private Integer riskId;
	@Column(name="VEH_NO")
	private String vehNo;
	@Column(name="COMP_VEH_IND")
	private String compVehInd;
	
	@Column(name="SEAT_CAPACITY")
	private Integer seatCapacity;

	@Column(name="VEH_MODEL")
	private String vehModel;
	@Column(name="QUOTATION_NO")
	private String quotationNo;
    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "QUOTATION_NO",insertable=false,updatable=false)
	private Transaction vehTransaction;
	public Integer getRiskId() {
		return riskId;
	}
	public void setRiskId(Integer riskId) {
		this.riskId = riskId;
	}
	public String getVehNo() {
		return vehNo;
	}
	public void setVehNo(String vehNo) {
		this.vehNo = vehNo;
	}
	public String getCompVehInd() {
		return compVehInd;
	}
	public void setCompVehInd(String compVehInd) {
		this.compVehInd = compVehInd;
	}
	public Integer getSeatCapacity() {
		return seatCapacity;
	}
	public void setSeatCapacity(Integer seatCapacity) {
		this.seatCapacity = seatCapacity;
	}
	public String getVehModel() {
		return vehModel;
	}
	public void setVehModel(String vehModel) {
		this.vehModel = vehModel;
	}
	public String getQuotationNo() {
		return quotationNo;
	}
	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}
	public Transaction getVehTransaction() {
		return vehTransaction;
	}
	public void setVehTransaction(Transaction vehTransaction) {
		this.vehTransaction = vehTransaction;
	}
	

	


}