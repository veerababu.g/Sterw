package com.sterw.roadwarrior.domain.model.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
/**
 * @author HCL
 *
 */
@JsonInclude(Include.NON_NULL)
public class BasicValueDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("Values")
	private String values;

	private String basicId;
	private String quotationNo;
	@JsonProperty("ValuesCategory")
	private String valuesCategory;

	public String getValues() {
		return values;
	}

	public void setValues(String values) {
		this.values = values;
	}

	public String getValuesCategory() {
		return valuesCategory;
	}

	public void setValuesCategory(String valuesCategory) {
		this.valuesCategory = valuesCategory;
	}

	public String getBasicId() {
		return basicId;
	}

	public void setBasicId(String basicId) {
		this.basicId = basicId;
	}

	public String getQuotationNo() {
		return quotationNo;
	}

	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}
	
}
