package com.sterw.roadwarrior.domain.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the "QTGC_COVER" database table.
 * 
 */
@Entity
@Table(name="QTGC_COVER")
public class Cover implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="COVER_ID")
	private Integer coverNumber;

	@Column(name="COMMISSION_PCT")
	private Integer commisionPct;
	
	@Column(name="RISK_ID")
	private String riskNo;

	@Column(name="COMMISSION_AMT")
	private BigDecimal commissionAmt;

	@Column(name="COVER_ANNUAL_PREM")
	private BigDecimal coverAnnualPrem;

	@Column(name="COVER_BASIC_PREM")
	private BigDecimal coverBasicPrem;

	@Column(name="COVER_CODE")
	private String coverCode;

	@Column(name="COVER_GROSS_PREM")
	private BigDecimal coverGrossPrem;

	@Column(name="COVER_NETT_PREM")
	private BigDecimal coverNettPrem;

	@Column(name="COVER_PLAN_CODE")
	private String coverPlanCode;

	@Column(name="COVER_SI")
	private BigDecimal coverSi;

	@Column(name="QUOTATION_NO")
	private String quotationNo;

	@Column(name="REBATE_AMT")
	private BigDecimal rebateAmt;

	@Column(name="REBATE_PCT")
	private Integer rebatePct;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumns({
		@JoinColumn(name="QUOTATION_NO",insertable=false,updatable=false)
		})
	private Transaction coverTransaction;
	@OneToMany(targetEntity=Benefit.class, fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumns({
		@JoinColumn(name="COVER_ID",referencedColumnName="COVER_ID"),
		})
	private Set<Benefit> qtgcBenefits;
	@OneToMany(targetEntity=CoverFee.class, fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="COVER_ID",referencedColumnName="COVER_ID")
	private Set<CoverFee> coverFee;

	@OneToMany(targetEntity=SubCover.class, fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="COVER_ID",referencedColumnName="COVER_ID")
	private Set<SubCover> subCover;
	
	
	
	public Set<SubCover> getSubCover() {
		return subCover;
	}

	public void setSubCover(Set<SubCover> subCover) {
		this.subCover = subCover;
	}

	public Integer getCommisionPct() {
		return commisionPct;
	}

	public Set<Benefit> getQtgcBenefits() {
		return qtgcBenefits;
	}

	public void setQtgcBenefits(Set<Benefit> qtgcBenefits) {
		this.qtgcBenefits = qtgcBenefits;
	}

	public String getRiskNo() {
		return riskNo;
	}

	public void setRiskNo(String riskNo) {
		this.riskNo = riskNo;
	}

	public void setCommisionPct(Integer commisionPct) {
		this.commisionPct = commisionPct;
	}

	

	
	
	public Transaction getCoverTransaction() {
		return coverTransaction;
	}

	public void setCoverTransaction(Transaction coverTransaction) {
		this.coverTransaction = coverTransaction;
	}

	

	public Set<CoverFee> getCoverFee() {
		return coverFee;
	}

	public void setCoverFee(Set<CoverFee> coverFee) {
		this.coverFee = coverFee;
	}

	

	public Integer getCoverNumber() {
		return coverNumber;
	}

	public void setCoverNumber(Integer coverNumber) {
		this.coverNumber = coverNumber;
	}

	public BigDecimal getCommissionAmt() {
		return commissionAmt;
	}

	public void setCommissionAmt(BigDecimal commissionAmt) {
		this.commissionAmt = commissionAmt;
	}

	public BigDecimal getCoverAnnualPrem() {
		return coverAnnualPrem;
	}

	public void setCoverAnnualPrem(BigDecimal coverAnnualPrem) {
		this.coverAnnualPrem = coverAnnualPrem;
	}

	public BigDecimal getCoverBasicPrem() {
		return coverBasicPrem;
	}

	public void setCoverBasicPrem(BigDecimal coverBasicPrem) {
		this.coverBasicPrem = coverBasicPrem;
	}

	public String getCoverCode() {
		return coverCode;
	}

	public void setCoverCode(String coverCode) {
		this.coverCode = coverCode;
	}

	public BigDecimal getCoverGrossPrem() {
		return coverGrossPrem;
	}

	public void setCoverGrossPrem(BigDecimal coverGrossPrem) {
		this.coverGrossPrem = coverGrossPrem;
	}

	public BigDecimal getCoverNettPrem() {
		return coverNettPrem;
	}

	public void setCoverNettPrem(BigDecimal coverNettPrem) {
		this.coverNettPrem = coverNettPrem;
	}

	public String getCoverPlanCode() {
		return coverPlanCode;
	}

	public void setCoverPlanCode(String coverPlanCode) {
		this.coverPlanCode = coverPlanCode;
	}

	public BigDecimal getCoverSi() {
		return coverSi;
	}

	public void setCoverSi(BigDecimal coverSi) {
		this.coverSi = coverSi;
	}

	public String getQuotationNo() {
		return quotationNo;
	}

	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}

	public BigDecimal getRebateAmt() {
		return rebateAmt;
	}

	public void setRebateAmt(BigDecimal rebateAmt) {
		this.rebateAmt = rebateAmt;
	}

	public Integer getRebatePct() {
		return rebatePct;
	}

	public void setRebatePct(Integer rebatePct) {
		this.rebatePct = rebatePct;
	}
}