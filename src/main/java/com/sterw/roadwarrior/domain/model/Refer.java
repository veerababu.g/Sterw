package com.sterw.roadwarrior.domain.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "QTGC_REFER" database table.
 * 
 */
@Entity
@Table(name="QTGC_REFER")

public class Refer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID")
	private Integer referId;
	
	@Column(name="QUOTATION_NO")
	private String quotationNo;
	
	@Column(name="REFER_CODE")
	private String referCode;

	
	
	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	 @JoinColumn(name="QUOTATION_NO",referencedColumnName="QUOTATION_NO",insertable=false,updatable=false)
	
	private Transaction qtgcTransaction;

	
	public String getQuotationNo() {
		return quotationNo;
	}

	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}

	public Integer getReferId() {
		return referId;
	}

	public void setReferId(Integer referId) {
		this.referId = referId;
	}

	public String getReferCode() {
		return referCode;
	}

	public void setReferCode(String referCode) {
		this.referCode = referCode;
	}

	public Transaction getQtgcTransaction() {
		return this.qtgcTransaction;
	}

	public void setQtgcTransaction(Transaction qtgcTransaction) {
		this.qtgcTransaction = qtgcTransaction;
	}

}