package com.sterw.roadwarrior.domain.model.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
@JsonInclude(Include.NON_NULL)
public class GetApiSignatureRequestDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("SubmissionNo")
	private String submissionNo;
	@JsonProperty("Amount")
	private String amount ;
	@JsonProperty("ApiSignature")
	private String apiSignature ;
	@JsonProperty("Code")
	private String code ;
	
	private String status;
	
	private String paymentId;
	
	
	
	/**
	 * @return the submissionNo
	 */
	public String getSubmissionNo() {
		return submissionNo;
	}
	/**
	 * @param submissionNo the submissionNo to set
	 */
	public void setSubmissionNo(String submissionNo) {
		this.submissionNo = submissionNo;
	}
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	/**
	 * @return the apiSignature
	 */
	public String getApiSignature() {
		return apiSignature;
	}
	/**
	 * @param apiSignature the apiSignature to set
	 */
	public void setApiSignature(String apiSignature) {
		this.apiSignature = apiSignature;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the paymentId
	 */
	public String getPaymentId() {
		return paymentId;
	}
	/**
	 * @param paymentId the paymentId to set
	 */
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	
	

}
