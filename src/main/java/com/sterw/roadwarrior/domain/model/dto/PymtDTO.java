package com.sterw.roadwarrior.domain.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * @author HCL
 *
 */
@JsonInclude(Include.NON_NULL)
public class PymtDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("SubmissionNumber")
	private String submissionNo;

	@JsonProperty("ErrorDescription")
	private String errDesc;
	@JsonProperty("MerchantCode")
	private String merchantCode;
	@JsonProperty("PaymentAmount")
	private BigDecimal pymtAmt;
	@JsonProperty("PaymentApprovalCode")
	private String pymtApprCode;
	@JsonProperty("PaymentDate")
	@JsonFormat
     (shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
	private Date pymtDate;
	@JsonProperty("PaymentStatus")
	private String pymtStatus;
	@JsonProperty("PaymentTxId")
	private String pymtTxId;
	@JsonProperty("QuotationNumber")
	private String quotationNo;
	
	@JsonProperty("SubmissionStatus")
	private String submissionStatus;
	
	@JsonProperty("PaymentId")
	private String pymtNumber;
	@JsonProperty("Referrer")
	private String referrer;
	@JsonProperty("Signature")
	private String signature;
	
	
	
	public String getSubmissionNo() {
		return submissionNo;
	}
	public void setSubmissionNo(String submissionNo) {
		this.submissionNo = submissionNo;
	}
	public String getErrDesc() {
		return errDesc;
	}
	public void setErrDesc(String errDesc) {
		this.errDesc = errDesc;
	}
	public String getMerchantCode() {
		return merchantCode;
	}
	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}
	public BigDecimal getPymtAmt() {
		return pymtAmt;
	}
	public void setPymtAmt(BigDecimal pymtAmt) {
		this.pymtAmt = pymtAmt;
	}
	public String getPymtApprCode() {
		return pymtApprCode;
	}
	public void setPymtApprCode(String pymtApprCode) {
		this.pymtApprCode = pymtApprCode;
	}
	public Date getPymtDate() {
		return pymtDate;
	}
	public void setPymtDate(Date pymtDate) {
		this.pymtDate = pymtDate;
	}
	public String getPymtStatus() {
		return pymtStatus;
	}
	public void setPymtStatus(String pymtStatus) {
		this.pymtStatus = pymtStatus;
	}
	public String getPymtTxId() {
		return pymtTxId;
	}
	public void setPymtTxId(String pymtTxId) {
		this.pymtTxId = pymtTxId;
	}
	public String getQuotationNo() {
		return quotationNo;
	}
	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}
	
	public String getPymtNumber() {
		return pymtNumber;
	}
	public void setPymtNumber(String pymtNumber) {
		this.pymtNumber = pymtNumber;
	}
	/**
	 * @return the submissionStatus
	 */
	public String getSubmissionStatus() {
		return submissionStatus;
	}
	/**
	 * @param submissionStatus the submissionStatus to set
	 */
	public void setSubmissionStatus(String submissionStatus) {
		this.submissionStatus = submissionStatus;
	}
	/**
	 * @return the referrer
	 */
	public String getReferrer() {
		return referrer;
	}
	/**
	 * @param referrer the referrer to set
	 */
	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}
	/**
	 * @return the signature
	 */
	public String getSignature() {
		return signature;
	}
	/**
	 * @param signature the signature to set
	 */
	public void setSignature(String signature) {
		this.signature = signature;
	}
	
	
	
	
}
