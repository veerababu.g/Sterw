package com.sterw.roadwarrior.domain.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;



/**
 * @author HCL
 *
 */
@JsonInclude(Include.NON_NULL)
public class TransactionDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty("QuotationNumber")
	private String quotationNo;
	@JsonProperty("ProductShortDescription")
	private String productShortDesc;
	@JsonProperty("QuotationProgress")
	private String quotationProgress;
	@JsonProperty("ProductCode")
	private String productCode;
	@JsonProperty("EffectiveDate")
	 @JsonFormat
     (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date effDate;
	@JsonProperty("ExpiryDate")
	 @JsonFormat
     (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date expDate;
	@JsonProperty("AgentCode")
	private String agentCode;
	@JsonProperty("Branch")
	private String branch;
	@JsonProperty("ClientID")
	private String clientNumber;
	@JsonProperty("ClientIDType")
	private String clientType;
	@JsonProperty("ClientName")
	private String clientName;
	@JsonProperty("Gender")
	private String gender;
	@JsonProperty("DOB")
	 @JsonFormat
     (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date dob;
	@JsonProperty("CompanyRegisteredDate")
	 @JsonFormat
     (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date compRegDate;
	@JsonProperty("Occupation")
	private String occupation;
	@JsonProperty("OccupationCode")
	private String occupationCode;
	
	@JsonProperty("MobileNumber")
	private BigDecimal mobileNo;
	@JsonProperty("EmailAddress")
	private String email;
	@JsonProperty("Address_1")
	private String address1;

	@JsonProperty("Address_2")
	private String address2;

	@JsonProperty("Address_3")
	private String address3;
	
	@JsonProperty("City")
	private String city;
	
	@JsonProperty("State")
	private String state;
	
	@JsonProperty("Country")
	private String country;
	@JsonProperty("GrossPremium")
	private BigDecimal grossPrem;
	@JsonProperty("AnnualPremium")
	private BigDecimal annualPrem;
	@JsonProperty("BasicPremium")
	private BigDecimal basicPrem;
	
	@JsonProperty("GSTAmount")
	private BigDecimal gstAmt;
	@JsonProperty("GSTPercentage")
	private BigDecimal gstPct;
	@JsonProperty("NettPremium")
	private BigDecimal nettPrem;
	@JsonProperty("PolicySumInsured")
	private BigDecimal policySi;
	@JsonProperty("PostCode")
	private Integer postcode;
	@JsonProperty("PremiumDue")
	private BigDecimal premDue;
	@JsonProperty("PremiumDueRounded")
	private BigDecimal premDueRounded;
	
	@JsonProperty("PremiumDueEcrptAmt")
	private String premiumDueEcrptAmt;
	
	@JsonProperty("RebateAmount")
	private BigDecimal rebateAmt;
	@JsonProperty("RebatePercentage")
	private BigDecimal rebatePct;
	@JsonProperty("CommAmt")
	private BigDecimal commissionAmt;
	
	@JsonProperty("Stamp")
	private BigDecimal stamp;
	@JsonProperty("UBBStatus")
	private String ubbStatus;
	
	@JsonProperty("CreatedDate")
	 @JsonFormat
     (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date createdDt;
	@JsonProperty("BasicValues")
	private Set<BasicValueDTO> qtgcBasicValues;
	@JsonProperty("Vehicle")
	private RiskVehDTO qtgcRiskVehs;
	@JsonProperty("Cover")
	private Set<CoverDTO> qtgcCovers;
	private PymtDTO  pymt; 
	public Date getCreatedDt() {
		return createdDt;
	}
	public BigDecimal getBasicPrem() {
		return basicPrem;
	}
	public void setBasicPrem(BigDecimal basicPrem) {
		this.basicPrem = basicPrem;
	}
	public void setCreatedDt(Date createdDt) {
		this.createdDt = createdDt;
	}
	public String getProductShortDesc() {
		return productShortDesc;
	}
	public void setProductShortDesc(String productShortDesc) {
		this.productShortDesc = productShortDesc;
	}
	public String getQuotationProgress() {
		return quotationProgress;
	}
	public void setQuotationProgress(String quotationProgress) {
		this.quotationProgress = quotationProgress;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public Date getEffDate() {
		return effDate;
	}
	public void setEffDate(Date effDate) {
		this.effDate = effDate;
	}
	public Date getExpDate() {
		return expDate;
	}
	
	public BigDecimal getAnnualPrem() {
		return annualPrem;
	}
	public void setAnnualPrem(BigDecimal annualPrem) {
		this.annualPrem = annualPrem;
	}
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
	public String getAgentCode() {
		return agentCode;
	}
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public Date getCompRegDate() {
		return compRegDate;
	}
	public void setCompRegDate(Date compRegDate) {
		this.compRegDate = compRegDate;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	
	
	public Set<BasicValueDTO> getQtgcBasicValues() {
		return qtgcBasicValues;
	}
	public void setQtgcBasicValues(Set<BasicValueDTO> qtgcBasicValues) {
		this.qtgcBasicValues = qtgcBasicValues;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getAddress3() {
		return address3;
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public BigDecimal getGrossPrem() {
		return grossPrem;
	}
	public void setGrossPrem(BigDecimal grossPrem) {
		this.grossPrem = grossPrem;
	}
	public BigDecimal getGstAmt() {
		return gstAmt;
	}
	public void setGstAmt(BigDecimal gstAmt) {
		this.gstAmt = gstAmt;
	}
	
	public BigDecimal getNettPrem() {
		return nettPrem;
	}
	public void setNettPrem(BigDecimal nettPrem) {
		this.nettPrem = nettPrem;
	}
	public BigDecimal getPolicySi() {
		return policySi;
	}
	public void setPolicySi(BigDecimal policySi) {
		this.policySi = policySi;
	}
	public Integer getPostcode() {
		return postcode;
	}
	public void setPostcode(Integer postcode) {
		this.postcode = postcode;
	}
	public BigDecimal getPremDue() {
		return premDue;
	}
	public void setPremDue(BigDecimal premDue) {
		this.premDue = premDue;
	}
	
	public BigDecimal getRebateAmt() {
		return rebateAmt;
	}
	public void setRebateAmt(BigDecimal rebateAmt) {
		this.rebateAmt = rebateAmt;
	}
	
	public String getUbbStatus() {
		return ubbStatus;
	}
	public void setUbbStatus(String ubbStatus) {
		this.ubbStatus = ubbStatus;
	}
	public String getQuotationNo() {
		return quotationNo;
	}
	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}
	public String getClientNumber() {
		return clientNumber;
	}
	public void setClientNumber(String clientNumber) {
		this.clientNumber = clientNumber;
	}
	public String getClientType() {
		return clientType;
	}
	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
	public RiskVehDTO getQtgcRiskVehs() {
		return qtgcRiskVehs;
	}
	public void setQtgcRiskVehs(RiskVehDTO qtgcRiskVehs) {
		this.qtgcRiskVehs = qtgcRiskVehs;
	}
	public Set<CoverDTO> getQtgcCovers() {
		return qtgcCovers;
	}
	public void setQtgcCovers(Set<CoverDTO> qtgcCovers) {
		this.qtgcCovers = qtgcCovers;
	}
	
	public String getOccupationCode() {
		return occupationCode;
	}
	public void setOccupationCode(String occupationCode) {
		this.occupationCode = occupationCode;
	}
	public PymtDTO getPymt() {
		return pymt;
	}
	public void setPymt(PymtDTO pymt) {
		this.pymt = pymt;
	}
	public BigDecimal getGstPct() {
		return gstPct;
	}
	public void setGstPct(BigDecimal gstPct) {
		this.gstPct = gstPct;
	}
	
	
	public BigDecimal getRebatePct() {
		return rebatePct;
	}
	public void setRebatePct(BigDecimal rebatePct) {
		this.rebatePct = rebatePct;
	}
	public BigDecimal getStamp() {
		return stamp;
	}
	public void setStamp(BigDecimal stamp) {
		this.stamp = stamp;
	}
	public BigDecimal getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(BigDecimal mobileNo) {
		this.mobileNo = mobileNo;
	}
	/**
	 * @return the commissionAmt
	 */
	public BigDecimal getCommissionAmt() {
		return commissionAmt;
	}
	/**
	 * @param commissionAmt the commissionAmt to set
	 */
	public void setCommissionAmt(BigDecimal commissionAmt) {
		this.commissionAmt = commissionAmt;
	}
	/**
	 * @return the premDueRounded
	 */
	public BigDecimal getPremDueRounded() {
		return premDueRounded;
	}
	/**
	 * @param premDueRounded the premDueRounded to set
	 */
	public void setPremDueRounded(BigDecimal premDueRounded) {
		this.premDueRounded = premDueRounded;
	}
	/**
	 * @return the premiumDueEcrptAmt
	 */
	public String getPremiumDueEcrptAmt() {
		return premiumDueEcrptAmt;
	}
	/**
	 * @param premiumDueEcrptAmt the premiumDueEcrptAmt to set
	 */
	public void setPremiumDueEcrptAmt(String premiumDueEcrptAmt) {
		this.premiumDueEcrptAmt = premiumDueEcrptAmt;
	}
	
	
}