package com.sterw.roadwarrior.domain.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
/**
 * @author HCL
 *
 */
@JsonInclude(Include.NON_NULL)
public class SubCoverDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("SubCoverId")
	private Integer subCoverNumber;
	
	@JsonProperty("PlanCode")
	private String planCode;

	@JsonProperty("SubCoverCode")
	private String subCoverCode;
	

	@JsonProperty("SubPremium")
	private BigDecimal subPremium;

	@JsonProperty("SubSumInsured")
	private BigDecimal subSi;

	@JsonProperty("CoverFeeList")
	private Set<CoverFeeDTO> subCoverFees;
	@JsonProperty("BenefitList")
	private Set<BenefitDTO> subCoverBenifit;

	

	public Integer getSubCoverNumber() {
		return subCoverNumber;
	}

	public void setSubCoverNumber(Integer subCoverNumber) {
		this.subCoverNumber = subCoverNumber;
	}

	public String getPlanCode() {
		return planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	public String getSubCoverCode() {
		return subCoverCode;
	}

	public void setSubCoverCode(String subCoverCode) {
		this.subCoverCode = subCoverCode;
	}

	public BigDecimal getSubPremium() {
		return subPremium;
	}

	public void setSubPremium(BigDecimal subPremium) {
		this.subPremium = subPremium;
	}

	public BigDecimal getSubSi() {
		return subSi;
	}

	public void setSubSi(BigDecimal subSi) {
		this.subSi = subSi;
	}

	public Set<CoverFeeDTO> getSubCoverFees() {
		return subCoverFees;
	}

	public void setSubCoverFees(Set<CoverFeeDTO> subCoverFees) {
		this.subCoverFees = subCoverFees;
	}

	public Set<BenefitDTO> getSubCoverBenifit() {
		return subCoverBenifit;
	}

	public void setSubCoverBenifit(Set<BenefitDTO> subCoverBenifit) {
		this.subCoverBenifit = subCoverBenifit;
	}



	
	
	

}
