package com.sterw.roadwarrior.domain.model.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
/**
 * @author HCL
 *
 */
@JsonInclude(Include.NON_NULL)
public class RiskVehDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer riskId;
	private String quotationNo;
	@JsonProperty("VehicleNo")
	private String vehNo;
	
	@JsonProperty("VehicleSeat")
	private Integer seatCapacity;
	
	@JsonProperty("VehicleModel")
	private String vehModel;
	
	@JsonProperty("CompanyVehicle")
	private String compVehInd;
	
	public String getVehNo() {
		return vehNo;
	}
	public Integer getSeatCapacity() {
		return seatCapacity;
	}
	public void setSeatCapacity(Integer seatCapacity) {
		this.seatCapacity = seatCapacity;
	}
	public String getVehModel() {
		return vehModel;
	}
	public void setVehModel(String vehModel) {
		this.vehModel = vehModel;
	}
	public void setVehNo(String vehNo) {
		this.vehNo = vehNo;
	}
	public Integer getRiskId() {
		return riskId;
	}
	public void setRiskId(Integer riskId) {
		this.riskId = riskId;
	}
	public String getQuotationNo() {
		return quotationNo;
	}
	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}
	/**
	 * @return the compVehInd
	 */
	public String getCompVehInd() {
		return compVehInd;
	}
	/**
	 * @param compVehInd the compVehInd to set
	 */
	public void setCompVehInd(String compVehInd) {
		this.compVehInd = compVehInd;
	}
	
	
}
