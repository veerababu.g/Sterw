package com.sterw.roadwarrior.domain.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the "QTGC_PYMT" database table.
 * 
 */
@Entity
@Table(name="QTGC_PYMT")
@IdClass(PaymtPK.class)
public class Pymt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="SUBMISSION_NO")
	private String submissionNo;
	
	@Column(name="ERR_DESC")
	private String errDesc;

	@Column(name="MERCHANT_CODE")
	private String merchantCode;

	@Column(name="PYMT_AMT")
	private BigDecimal pymtAmt;
	
	@Column(name="PYMT_ID")
	private String pymtNumber;
	
	

	@Column(name="PYMT_APPR_CODE")
	private String pymtApprCode;

	
	@Column(name="PYMT_DATE")
	private Timestamp pymtDate;

	@Column(name="PYMT_STATUS")
	private String pymtStatus;

	@Column(name="PYMT_TX_ID")
	private String pymtTxId;
	@Column(name="QUOTATION_NO")
	private String quotationNo;
	@OneToOne
	@JoinColumn(name="QUOTATION_NO",insertable=false,updatable=false)
	private Transaction pymtTransaction;

	public String getQuotationNo() {
		return quotationNo;
	}

	public String getSubmissionNo() {
		return submissionNo;
	}
	public void setSubmissionNo(String submissionNo) {
		this.submissionNo = submissionNo;
	}

	public String getPymtNumber() {
		return pymtNumber;
	}

	public void setPymtNumber(String pymtNumber) {
		this.pymtNumber = pymtNumber;
	}

	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}
	
	public String getErrDesc() {
		return this.errDesc;
	}

	public void setErrDesc(String errDesc) {
		this.errDesc = errDesc;
	}

	public String getMerchantCode() {
		return this.merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public BigDecimal getPymtAmt() {
		return this.pymtAmt;
	}

	public void setPymtAmt(BigDecimal pymtAmt) {
		this.pymtAmt = pymtAmt;
	}

	public String getPymtApprCode() {
		return this.pymtApprCode;
	}

	public void setPymtApprCode(String pymtApprCode) {
		this.pymtApprCode = pymtApprCode;
	}

	
	/**
	 * @return the pymtDate
	 */
	public Timestamp getPymtDate() {
		return pymtDate;
	}

	/**
	 * @param pymtDate the pymtDate to set
	 */
	public void setPymtDate(Timestamp pymtDate) {
		this.pymtDate = pymtDate;
	}

	public String getPymtStatus() {
		return this.pymtStatus;
	}

	public void setPymtStatus(String pymtStatus) {
		this.pymtStatus = pymtStatus;
	}

	public String getPymtTxId() {
		return this.pymtTxId;
	}

	public void setPymtTxId(String pymtTxId) {
		this.pymtTxId = pymtTxId;
	}
	public Transaction getPymtTransaction() {
		return pymtTransaction;
	}
	public void setPymtTransaction(Transaction pymtTransaction) {
		this.pymtTransaction = pymtTransaction;
	}
	
}