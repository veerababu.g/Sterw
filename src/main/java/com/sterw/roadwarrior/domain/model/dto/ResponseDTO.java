package com.sterw.roadwarrior.domain.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author HCL
 *
 * @param <T>
 */
public class ResponseDTO<T> {
@JsonProperty("Status")
private String status;
@JsonProperty("Result")
private  T result;
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}

public T getResult() {
	return result;
}
public void setResult(T result) {
	this.result = result;
}

}
