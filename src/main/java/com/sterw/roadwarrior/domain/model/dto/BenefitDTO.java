package com.sterw.roadwarrior.domain.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
/**
 * @author HCL
 *
 */
@JsonInclude(Include.NON_NULL)
public class BenefitDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("CoverId")
	private Integer coverNO;
	
	@JsonProperty("SubCoverId")
	private Integer subCoverNO;
	
	
	
	@JsonProperty("BenefitCode")
	private String benefitCode;

	@JsonProperty("BenefitPremium")
	private BigDecimal benefitPrem;

	@JsonProperty("BenefitSumInsured")
	private BigDecimal benefitSi;

	public String getBenefitCode() {
		return benefitCode;
	}

	public void setBenefitCode(String benefitCode) {
		this.benefitCode = benefitCode;
	}

	public BigDecimal getBenefitPrem() {
		return benefitPrem;
	}

	public void setBenefitPrem(BigDecimal benefitPrem) {
		this.benefitPrem = benefitPrem;
	}

	public BigDecimal getBenefitSi() {
		return benefitSi;
	}

	public void setBenefitSi(BigDecimal benefitSi) {
		this.benefitSi = benefitSi;
	}

	public Integer getCoverNO() {
		return coverNO;
	}

	public void setCoverNO(Integer coverNO) {
		this.coverNO = coverNO;
	}

	public Integer getSubCoverNO() {
		return subCoverNO;
	}

	public void setSubCoverNO(Integer subCoverNO) {
		this.subCoverNO = subCoverNO;
	}

	

	

	
	

	
}
