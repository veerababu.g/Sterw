package com.sterw.roadwarrior.domain.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * The persistent class for the "QTGC_BENEFIT" database table.
 * 
 */
@Entity
@Table(name="QTGC_BENEFIT")

public class Benefit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID")
	private Integer benefitId;
	
	@Column(name="BENEFIT_CODE")
	private String benefitCode;
	
	@Column(name="SUB_COVER_ID",insertable=false,updatable=false)
	private String subCoverNO;
	@Column(name="COVER_ID",insertable=false,updatable=false)
	private Integer coverNO;
	@Column(name="BENEFIT_PREM")
	private BigDecimal benefitPrem;

	@Column(name="BENEFIT_SI")
	private BigDecimal benefitSi;
		@ManyToOne(fetch=FetchType.LAZY,cascade=CascadeType.REFRESH)
	@JoinColumns({
		@JoinColumn(name="COVER_ID",referencedColumnName="COVER_ID")
		})
	private Cover cover;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumns({
		
		@JoinColumn(name="SUB_COVER_ID",referencedColumnName="SUB_COVER_ID")
		
	})
	private SubCover subCover;

	public Cover getCover() {
		return cover;
	}

	public void setCover(Cover cover) {
		this.cover = cover;
	}

	
	public Integer getBenefitId() {
		return benefitId;
	}

	public void setBenefitId(Integer benefitId) {
		this.benefitId = benefitId;
	}

	public String getBenefitCode() {
		return benefitCode;
	}
	public void setBenefitCode(String benefitCode) {
		this.benefitCode = benefitCode;
	}
	public BigDecimal getBenefitPrem() {
		return this.benefitPrem;
	}

	public void setBenefitPrem(BigDecimal benefitPrem) {
		this.benefitPrem = benefitPrem;
	}

	public BigDecimal getBenefitSi() {
		return this.benefitSi;
	}

	public void setBenefitSi(BigDecimal benefitSi) {
		this.benefitSi = benefitSi;
	}
	public SubCover getSubCover() {
		return subCover;
	}

	public void setSubCover(SubCover subCover) {
		this.subCover = subCover;
	}

	public String getSubCoverNO() {
		return subCoverNO;
	}

	public void setSubCoverNO(String subCoverNO) {
		this.subCoverNO = subCoverNO;
	}

	public Integer getCoverNO() {
		return coverNO;
	}

	public void setCoverNO(Integer coverNO) {
		this.coverNO = coverNO;
	}


}