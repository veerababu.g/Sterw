package com.sterw.roadwarrior.domain.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
/**
 * @author HCL
 *
 */
@JsonInclude(Include.NON_NULL)
public class SubBenefitDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("Code")
	private String benefitCode;
	
	@JsonProperty("CoverId")
	private Integer coverNumber;
	
	@JsonProperty("SubCoverId")
	private Integer subCoverNumber;

	@JsonProperty("Premium")
	private BigDecimal benefitPrem;

	@JsonProperty("SumInsured")
	private BigDecimal benefitSi;

	public String getBenefitCode() {
		return benefitCode;
	}

	public void setBenefitCode(String benefitCode) {
		this.benefitCode = benefitCode;
	}

	public BigDecimal getBenefitPrem() {
		return benefitPrem;
	}

	public void setBenefitPrem(BigDecimal benefitPrem) {
		this.benefitPrem = benefitPrem;
	}

	public BigDecimal getBenefitSi() {
		return benefitSi;
	}

	public void setBenefitSi(BigDecimal benefitSi) {
		this.benefitSi = benefitSi;
	}

	public Integer getCoverNumber() {
		return coverNumber;
	}

	public void setCoverNumber(Integer coverNumber) {
		this.coverNumber = coverNumber;
	}

	public Integer getSubCoverNumber() {
		return subCoverNumber;
	}

	public void setSubCoverNumber(Integer subCoverNumber) {
		this.subCoverNumber = subCoverNumber;
	}

	

	
}
