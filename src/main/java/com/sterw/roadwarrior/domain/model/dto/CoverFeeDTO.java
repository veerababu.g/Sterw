package com.sterw.roadwarrior.domain.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
/**
 * @author HCL
 *
 */
@JsonInclude(Include.NON_NULL)
public class CoverFeeDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("CoverId")
	private Integer coverNumber;
	
	@JsonProperty("SubCoverId")
	private Integer subCoverNumber;
	
	
	
	public Integer getCoverNumber() {
		return coverNumber;
	}

	public void setCoverNumber(Integer coverNumber) {
		this.coverNumber = coverNumber;
	}

	@JsonProperty("FeeId")
	private String feeNumber;


	@JsonProperty("FeeAmt")
	private BigDecimal feeAmount;

	@JsonProperty("FeeCode")
	private String feeCode;

	

	public String getFeeNumber() {
		return feeNumber;
	}

	public void setFeeNumber(String feeNumber) {
		this.feeNumber = feeNumber;
	}

	public BigDecimal getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}

	public String getFeeCode() {
		return feeCode;
	}

	public void setFeeCode(String feeCode) {
		this.feeCode = feeCode;
	}

	public Integer getSubCoverNumber() {
		return subCoverNumber;
	}

	public void setSubCoverNumber(Integer subCoverNumber) {
		this.subCoverNumber = subCoverNumber;
	}

	
	
	
}
