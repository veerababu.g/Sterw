package com.sterw.roadwarrior.domain.model.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * @author HCL
 *
 */
@JsonInclude(Include.NON_NULL)

public class DuplicateCustomerRequestDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@JsonProperty("ClientID")
	private String clientId;
	
	@JsonProperty("ProductCode")
	private String productCode;
	
	@JsonProperty("EffectiveDate")
	 @JsonFormat
   (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date effDate;
	
	@JsonProperty("ExpiryDate")
	 @JsonFormat
   (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date expDate;
	
	@JsonProperty("VehicleNumber")
	private String vehNo;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Date getEffDate() {
		return effDate;
	}

	public void setEffDate(Date effDate) {
		this.effDate = effDate;
	}

	public Date getExpDate() {
		return expDate;
	}

	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}

	public String getVehNo() {
		return vehNo;
	}

	public void setVehNo(String vehNo) {
		this.vehNo = vehNo;
	}

}
