package com.sterw.roadwarrior.domain.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


/**
 * The persistent class for the "QTGC_POLICY" database table.
 * 
 */
@Entity
@Table(name="QTGC_POLICY")
public class Policy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ID")
	private Integer policyId;
	
	@Column(name="POLICY_NO")
	private String policyNo;

	@Column(name="QUOTATION_NO")
	private String quotationNo;
	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	 @JoinColumn(name="QUOTATION_NO",referencedColumnName="QUOTATION_NO",insertable=false,updatable=false)
	private Transaction policyTransaction;

	
	public String getQuotationNo() {
		return quotationNo;
	}
	public void setQuotationNo(String quotationNo) {
		this.quotationNo = quotationNo;
	}

	public Integer getPolicyId() {
		return policyId;
	}

	public void setPolicyId(Integer policyId) {
		this.policyId = policyId;
	}
	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

}